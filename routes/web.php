<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserCommentsController;
use App\Http\Controllers\UserFavoritesController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/






Route::middleware(['auth','verified'])->group(function(){

    Route::controller(NewsController::class)->group(function(){
        Route::get('/', 'index')->name('/');
        Route::get('news/{url}', 'get')->name('news.show');
        Route::get('/all-news', 'allNews')->name('all.news');

    });

    Route::controller(UserFavoritesController::class)->group(function(){
        Route::delete('users/favorites', 'delete')->name('delete.news');
        Route::get('users/favorites', 'get')->name('get.favorites');
        Route::post('users/favorites', 'create')->name('save.news');
    });

    Route::controller(UserCommentsController::class)->group(function(){
        Route::post('/comments', 'store')->name('comments.store');
        Route::delete('/comments/{id}', 'delete')->name('delete.comment');
        Route::put('/comments/{id}', 'update')->name('update.comment');
    });

    Route::controller(ProfileController::class)->group(function(){
        Route::get('/profile', 'edit')->name('profile.edit');
        Route::patch('/profile', 'update')->name('profile.update');
        Route::delete('/profile', 'delete')->name('profile.destroy');
    });



});

    Route::middleware(['admin', 'auth', 'verified'])->group(function(){
        Route::controller(AdminController::class)->group(function(){
            Route::get('/profile/admin', 'panel')->name('profile.admin');
            Route::get('admin/users/{id}/comments', 'comments')->name('admin.userComments');
            Route::put('admin/users/{id}/comments', 'updateComment')->name('admin.userComments.update');
            Route::delete('admin/users/{id}/comments', 'deleteComment')->name('admin.userComments.delete');
            Route::get('admin/users/{id}/favorites', 'favorites')->name('admin.userFavorites');
            Route::delete('admin/users/{id}/favorites', 'deleteFavorite')->name('admin.userFavorites.delete');
            Route::get('admin/users/{id}/logs', 'logs')->name('admin.userLogs');
            Route::get('admin/users/{id}', 'information')->name('admin.userInfo');
            Route::delete('admin/users/{id}', 'delete')->name('admin.profile.destroy');
            Route::patch('admin/users/{id}', 'update')->name('admin.profile.update');
        });


});


require __DIR__.'/auth.php';
