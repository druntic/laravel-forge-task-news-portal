<?php

use App\Models\Logs;
use App\Models\News;
use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\RefreshDatabase;


test('user can add to favorites', function () {

    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Make the request with fake news data
    $response = $this->postJson(route('save.news'), [
        'news_url' => 'https://example.com/news/1',
        'news_title' => 'Fake News',
        'news_image' => 'https://example.com/news/1/image.jpg',
        'news_description' => 'This is a fake news article.',
    ]);

    // Assert the news record is created in the database
    $this->assertDatabaseHas('user_favorites_table', [
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
        'user_id' => $user->id,
    ]);

    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $user->id,
        'action' => 'favorited',
        'description' => "User favorited Fake News",
    ]);

    // Assert that only one log record is created
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response->assertSessionHasNoErrors();
});

test('user favorites table exists', function () {
    // Assert that the news_favorites table exists in the database
    $this->assertTrue(Schema::hasTable('user_favorites_table'));

});


test('user can remove from favorites', function () {

    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Create a fake news record in the database
    $news = News::factory()->create([
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
        'user_id' => $user->id,
    ]);


    // Make the request with the news id
    $response = $this->deleteJson(route('delete.news') , ['id' => $news->id]);

    // Assert the news record is missing from the database
    $this->assertDatabaseMissing('user_favorites_table', [
        'id' => $news->id,
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
        'user_id' => $user->id,
    ]);
    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $user->id,
        'action' => 'delete_favorite',
        'description' => "User removed Fake News from favorites",
    ]);

    // Assert that only one log record is created
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response->assertSessionHasNoErrors();
});

test('admin can remove news from user favorites', function () {

    // Create a normal user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Create a fake news record in the database
    $news = News::factory()->create([
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'image' => 'https://example.com/news/2/image.jpg',
        'description' => 'This is another fake news article.',
        'user_id' => $user->id,
    ]);

    // Make the request with fake news data
    $response = $this->postJson(route('save.news'), [
        'news_url' => 'https://example.com/news/1',
        'news_title' => 'Fake News',
        'news_image' => 'https://example.com/news/1/image.jpg',
        'news_description' => 'This is a fake news article.',
    ]);

    // Create an admin user using a model factory
    $admin = User::factory()->create(['is_admin' => true]);

    // Authenticate the admin user
    $this->actingAs($admin);

    // Make the request with the news id and the user id
    $response = $this->deleteJson(route('delete.news') , ['id' => $news->id]);

    // Assert the news record is missing from the database
    $this->assertDatabaseMissing('user_favorites_table', [
        'id' => $news->id,
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
        'user_id' => $user->id,
    ]);
    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $admin->id,
        'action' => 'delete_favorite',
        'description' => "User removed Another Fake News from favorites",
    ]);

    // Assert that only one log record is created(create and delete)
    $this->assertDatabaseCount('user_logs_table', 2);

    // Assert there are no validation errors in the session
    $response->assertSessionHasNoErrors();
});


test('check if a user can add the same news only once', function () {
    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate user and add a news to their favorites
    $this->actingAs($user);
    $this->postJson(route('save.news'), [
        'news_url' => 'https://example.com/news/1',
        'news_title' => 'Fake News',
        'news_image' => 'https://example.com/news/1/image.jpg',
        'news_description' => 'This is a fake news article.',
    ]);

    // Try to add the same news to their favorites again
    $this->actingAs($user);
    $this->postJson(route('save.news'), [
        'news_url' => 'https://example.com/news/1',
        'news_title' => 'Fake News',
        'news_image' => 'https://example.com/news/1/image.jpg',
        'news_description' => 'This is a fake news article.',
    ]);

    $this->assertEquals(1, $user->news_favorites()->count());
});

test('check if a guest can add to favorites if they are not logged in', function (){
    // Send a POST request to add the news article to favorites as a guest
    $this->postJson(route('save.news'), [
        'news_url' => 'https://example.com/news/1',
        'news_title' => 'Fake News',
        'news_image' => 'https://example.com/news/1/image.jpg',
        'news_description' => 'This is a fake news article.',
    ]);


    // Assert that the news article is not added to favorites
    $this->assertDatabaseMissing('user_favorites_table', [
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
    ]);
});

test('user cannot remove the same news from favorites twice', function () {
    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Create a fake news record in the database
    $news = News::factory()->create([
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
        'user_id' => $user->id,
    ]);

    // Make the request with the news id for the first time
    $response1 = $this->deleteJson(route('delete.news') , ['id' => $news->id]);

    // Assert the news record is missing from the database
    $this->assertDatabaseMissing('user_favorites_table', [
        'id' => $news->id,
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
        'user_id' => $user->id,
    ]);
    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $user->id,
        'action' => 'delete_favorite',
        'description' => "User removed Fake News from favorites",
    ]);

    // Assert that only one log record is created
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response1->assertSessionHasNoErrors();

    // Make the request with the same news id for the second time
    $response2 = $this->deleteJson(route('delete.news') , ['id' => $news->id]);

    // Assert the news record is still missing from the database
    $this->assertDatabaseMissing('user_favorites_table', [
        'id' => $news->id,
        'url' => 'https://example.com/news/1',
        'title' => 'Fake News',
        'image' => 'https://example.com/news/1/image.jpg',
        'description' => 'This is a fake news article.',
        'user_id' => $user->id,
    ]);

    // Assert that no log record is created for the second time removal
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response2->assertSessionHasNoErrors();

});

