<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;

test('profile page is displayed', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->get('/profile');

    $response->assertOk();
});

test('profile information can be updated', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->patch('/profile', [
            'name' => 'Test User',
            'email' => 'test@example.com',
            'language' => $user->language,
            'favoriteCategory' => $user->favoriteCategory,
            'country' => $user->country
        ]);

    $response
        ->assertSessionHasNoErrors();

    $user->refresh();

    $this->assertSame('Test User', $user->name);
    $this->assertSame('test@example.com', $user->email);
});

test('email verification status is unchanged when the email address is unchanged', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->patch('/profile', [
            'name' => 'Test User',
            'email' => $user->email,
            'language' => $user->language,
            'favoriteCategory' => $user->favoriteCategory,
            'country' => $user->country
        ]);

    $response
        ->assertSessionHasNoErrors();

    $this->assertNotNull($user->refresh()->email_verified_at);
});

test('user can delete their account', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->delete('/profile', [
            'password' => 'password',
        ]);

    $response
        ->assertSessionHasNoErrors()
        ->assertRedirect('/');

    $this->assertGuest();
    $this->assertNull($user->fresh());
});

test('correct password must be provided to delete account', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->from('/profile')
        ->delete('/profile', [
            'password' => 'wrong-password',
        ]);


    $this->assertNotNull($user->fresh());


});

test('user can change his language', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->patch('/profile', [
            'name' => $user->name,
            'email' => $user->email,
            'language' => 'en',
            'favoriteCategory' => $user->favoriteCategory,
            'country' => $user->country
        ]);

    $response
        ->assertSessionHasNoErrors();

    $user->refresh();

    $this->assertSame('en', $user->language);
});

test('user can change his country', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->patch('/profile', [
            'name' => $user->name,
            'email' => $user->email,
            'language' => $user->language,
            'favoriteCategory' => $user->favoriteCategory,
            'country' => 'us'
        ]);

    $response
        ->assertSessionHasNoErrors();

    $user->refresh();

    $this->assertSame('us', $user->country);
});

test('user can change his favorite category', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->patch('/profile', [
            'name' => $user->name,
            'email' => $user->email,
            'language' => $user->language,
            'favoriteCategory' => 'sports',
            'country' => $user->country
        ]);

    $response
        ->assertSessionHasNoErrors();

    $user->refresh();

    $this->assertSame('sports', $user->favoriteCategory);
});

test('user can change his email', function () {
    $user = User::factory()->create();

    $response = $this
        ->actingAs($user)
        ->patch('/profile', [
            'name' => $user->name,
            'email' => 'brate@odmah.com',
            'language' => $user->language,
            'favoriteCategory' => $user->favoriteCategory,
            'country' => $user->country
        ]);

    $response
        ->assertSessionHasNoErrors();

    $user->refresh();

    $this->assertSame('brate@odmah.com', $user->email);
});

test('user cannot set favorite category as empty', function () {
    $user = User::factory()->create();

    $response = $this->actingAs($user)
        ->from(route('profile.edit'))
        ->patch(route('profile.update'), [
            'email' => $user->email,
            'favoriteCategory' => '',
            'country' => $user->country,
            'language' => $user->language,
        ]);


    $user->refresh();
    $this->assertNotEquals('', $user->favoriteCategory);
});

test('user cannot set his language as empty', function () {
    $user = User::factory()->create();

    $response = $this->actingAs($user)
        ->from(route('profile.edit'))
        ->patch(route('profile.update'), [
            'email' => $user->email,
            'favoriteCategory' => $user->favoriteCategory,
            'country' => $user->country,
            'language' => null,
        ]);


    $user->refresh();
    $this->assertNotEquals(null, $user->language);
});
