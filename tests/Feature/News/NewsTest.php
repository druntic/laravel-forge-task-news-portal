<?php

use App\Models\User;
use Illuminate\Http\Request;

test('main page can be loeaded', function () {
    $user = User::factory()->create();
    // Make a GET request to the / route
    $response = $this->actingAs($user)->get('/');

    // Assert that the response status is 200 (OK)
    $response->assertStatus(200);

});


test('all news page can be loeaded', function () {
    $user = User::factory()->create();
    // Make a GET request to the / route
    $response = $this->actingAs($user)->get('/all-news');

    // Assert that the response status is 200 (OK)
    $response->assertStatus(200);

});

test('check if the search parameter is news by default', function(){
    $user = User::factory()->create();
    // Make a GET request to the / route
    $response = $this->actingAs($user)->get('/');
    // Check that the response was generated successfully
    $response->assertStatus(200);

    // Check that the q parameter defaults to 'news'
    $this->assertEquals('news', $response->original->getData()['search']);
});

test('check if the search parameter can be modified ', function(){
    $user = User::factory()->create();
    // Make a GET request to the / route
    $response = $this->actingAs($user)->get('/?search=example');
    // Check that the response was generated successfully
    $response->assertStatus(200);

    // Check that the q parameter defaults to 'news'
    $this->assertEquals('example',  $response->original->getData()['search']);
});

test('check if the category defaults to the users favorite category ', function(){
    $user = User::factory()->create();
    $favorite_category = $user->favoriteCategory;

    // Make a GET request to the route that corresponds to the index method, without passing a category
    $response = $this->actingAs($user)->get('/');

    // Check that the response was generated successfully
    $response->assertStatus(200);

    // Assert that the category parameter defaults to the user's favorite category
    $this->assertEquals($favorite_category, $response->original->getData()['category']);
});


test('check if the category can be modified ', function(){
    $user = User::factory()->create();
    $category = 'sports';

    // Make a GET request to the route that corresponds to the index method, without passing a category
    $response = $this->actingAs($user)->get("/?category=$category");

    // Check that the response was generated successfully
    $response->assertStatus(200);

    // Assert that the category parameter defaults to the user's favorite category
    $this->assertEquals($category, $response->original->getData()['category']);
});

test('check if the default sources are null when not checked ', function(){
    $user = User::factory()->create();


    $response = $this->actingAs($user)->get("/");

    // Check that the response was generated successfully
    $response->assertStatus(200)
             ->assertViewHas('source', null);
});

test('check if sort is set to null if we dont select it', function(){
    $user = User::factory()->create();

    $this->actingAs($user);
    $response = $this->get('/');
    $response->assertStatus(200);

    // Set the sort parameter to "none"
    $response = $this->get('/?sort=none');
    $response->assertStatus(200);

    // Assert that the sort_by parameter is null
    $response->assertViewHas('sort', null);
});


test('check if sort can be modified', function(){
    $user = User::factory()->create();

    $this->actingAs($user);
    $response = $this->get('/');
    $response->assertStatus(200);
    $sort = 'publishedAt';
    // Set the sort parameter to "none"
    $response = $this->get("/?sort=$sort");

    $response->assertOk();


    // Assert that the sort_by parameter is $sort
    $response->assertViewHas('sort', $sort);
});
