<?php

use App\Models\User;
use App\Models\UserComment;
test('admin can enter admin panel', function () {

    // Create a user with is_admin set to true
    $user = User::factory()->create(['is_admin' => true]);

    // Log in as the admin user
    $this->actingAs($user);

    // Make a request to access the admin panel route
    $response = $this->get(route('profile.admin'));

    // Assert that the response status is 200 (OK)
    $response->assertStatus(200);

    // Assert that the response view is 'panel'
    $response->assertViewIs('profile.admin.admin');
});

test('user cant enter admin panel', function () {

    // Create a user with is_admin set to false
    $user = User::factory()->create(['is_admin' => false]);

    // Log in as the normal user
    $this->actingAs($user);

    // Make a request to access the admin panel route
    $response = $this->get(route('profile.admin'));

    // Assert that the response is a redirect to the index route
    $response->assertRedirect(route('/'));
});

test('admin can see activity log of users', function () {

    // Create a user with is_admin set to true
    $admin = User::factory()->create(['is_admin' => true]);
    // Create normal user
    $user = User::factory()->create();

    // Log in as the admin user
    $this->actingAs($admin);

    // Make a request to access the user logs route with the user id
    $response = $this->get(route('admin.userLogs', $user->id));

    // Assert that the response status is 200 (OK)
    $response->assertStatus(200);

    // Assert that the response view is 'profile.admin.userLogs'
    $response->assertViewIs('profile.admin.userLogs');
});

test('admin can view all comments made by a user', function () {

    // Create a user with is_admin set to true
    $admin = User::factory()->create(['is_admin' => true]);
    // Create a user using a model factory
    $user = User::factory()->create();
    // Authenticate the user
    $this->actingAs($user);

    // Make the request with fake comment data
    $response = $this->postJson(route('comments.store'), [
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
    ]);

    // Assert the comment record is created in the database
    $this->assertDatabaseHas('user_comments_table', [
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);

    // Log in as the admin user
    $this->actingAs($admin);

    // Make a request to access the user's comments route with the user id
    $response = $this->get(route('admin.userComments', $user->id));

    // Assert that the response status is 200 (OK)
    $response->assertStatus(200);

    // Assert that the response view is 'profile.admin.userComments'
    $response->assertViewIs('profile.admin.userComments');

});


test('normal user cant see activity log of users', function () {

    // Create normal user
    $user = User::factory()->create();

    // Log in as the admin user
    $this->actingAs($user);

    // Make a request to access the user logs route with the user id
    $response = $this->get(route('admin.userLogs', $user->id));

    // Assert that the response is a redirect to the index route
    $response->assertRedirect(route('/'));
});

test('admin can change users category', function () {
    // Create an admin and a regular user and log in as admin
    $admin = User::factory()->create(['is_admin' => true]);
    $user = User::factory()->create();
    $this->actingAs($admin);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user->id]), [
        'email' => $user->email,
        'language' => $user->language,
        'country' => $user->country,
        'favoriteCategory' => 'sports',
    ]);

    // Assert that the response is successful and redirects back
    $response->assertStatus(302);

    $this->assertSame('sports', $user->fresh()->favoriteCategory);
});

test('normal user cant another users category', function () {
    // Create two regular users
    $user1 = User::factory()->create();
    $user2 = User::factory()->create();
    $this->actingAs($user1);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user2->id]), [
        'email' => $user2->email,
        'language' => $user2->language,
        'country' => $user2->country,
        'favoriteCategory' => 'sports',
    ]);


    $this->assertNotSame('sports', $user2->fresh()->favoriteCategory);
});

test('admin can change users language', function () {
    // Create an admin and a regular user and log in as admin
    $admin = User::factory()->create(['is_admin' => true]);
    $user = User::factory()->create();
    $this->actingAs($admin);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user->id]), [
        'email' => $user->email,
        'language' => 'ar',
        'country' => $user->country,
        'favoriteCategory' => $user->favoriteCategory,
    ]);

    // Assert that the response is successful and redirects back
    $response->assertStatus(302);

    $this->assertSame('ar', $user->fresh()->language);
});

test('normal user cant another users language', function () {
    // Create two regular users
    $user1 = User::factory()->create();
    $user2 = User::factory()->create();
    $this->actingAs($user1);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user2->id]), [
        'email' => $user2->email,
        'language' => 'ar',
        'country' => $user2->country,
        'favoriteCategory' => $user2->favoriteCategory,
    ]);

    $this->assertNotSame('ar', $user2->fresh()->language);
});

test('admin can change users country', function () {
    // Create an admin and a regular user and log in as admin
    $admin = User::factory()->create(['is_admin' => true]);
    $user = User::factory()->create();
    $this->actingAs($admin);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user->id]), [
        'email' => $user->email,
        'language' => $user->language,
        'country' => 'cu',
        'favoriteCategory' => $user->favoriteCategory,
    ]);


    // Assert that the response is successful and redirects back
    $response->assertStatus(302);

    $this->assertSame('cu', $user->fresh()->country);
});

test('normal user cant another users country', function () {
    // Create two regular users
    $user1 = User::factory()->create();
    $user2 = User::factory()->create();
    $this->actingAs($user1);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user2->id]), [
        'email' => $user2->email,
        'language' => $user2->language,
        'country' => 'cu',
        'favoriteCategory' => $user2->favoriteCategory,
    ]);

    $this->assertNotSame('cu', $user2->fresh()->country);
});


test('admin can change users name', function () {
    // Create an admin and a regular user and log in as admin
    $admin = User::factory()->create(['is_admin' => true]);
    $user = User::factory()->create();
    $this->actingAs($admin);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user->id]), [
        'name' => 'pero',
        'email' => $user->email,
        'language' => $user->language,
        'country' => $user->country,
        'favoriteCategory' => $user->favoriteCategory,
    ]);


    // Assert that the response is successful and redirects back
    $response->assertStatus(302);

    $this->assertSame('pero', $user->fresh()->name);
});

test('normal user cant another users name', function () {
    // Create two regular users
    $user1 = User::factory()->create();
    $user2 = User::factory()->create();
    $this->actingAs($user1);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('admin.profile.update', ['id' => $user2->id]), [
        'name' => 'pero',
        'email' => $user2->email,
        'language' => $user2->language,
        'country' => $user2->country,
        'favoriteCategory' => $user2->favoriteCategory,
    ]);

    $this->assertNotSame('pero', $user2->fresh()->name);
});


test('admin can not change users email', function () {
    // Create an admin and a regular user and log in as admin
    $admin = User::factory()->create(['is_admin' => true]);
    $user = User::factory()->create();
    $this->actingAs($admin);

    // Make a request to update the user's profile with valid data
    $response = $this->patch(route('profile.update'), [
        'id' => $user->id,
        'email' => 'test@aa.com',
        'language' => $user->language,
        'country' => $user->country,
        'favoriteCategory' => $user->favoriteCategory,
    ]);



    // Assert that the response is successful and redirects back
    $response->assertStatus(302);

    $this->assertNotSame('test@aa.com', $user->fresh()->email);
});

test('admin can delete a users profile', function () {
    // Create an admin user
    $admin = User::factory()->create(['is_admin' => true]);

    // Create a regular user
    $user = User::factory()->create();

    // Send a DELETE request to the delete user route with the admin's credentials
    $response = $this->actingAs($admin)->delete(route('admin.profile.destroy', $user->id));

    // Assert that the response has a 302 status code (redirect)
    $response->assertStatus(302);

    // Assert that the user was deleted from the database
    $this->assertDatabaseMissing('users', ['id' => $user->id]);
});

test('normal user cant delete another users profile', function () {

    // Create a regular user1
    $user1 = User::factory()->create();
    // Create a regular user2
    $user2 = User::factory()->create();

    // Send a DELETE request to the delete user2 route with user1 credentials
    $response = $this->actingAs($user1)->delete(route('admin.profile.destroy', $user2->id));

    // Assert that the response has a 302 status code (redirect)
    $response->assertStatus(302);

    // Assert that the admin's profile still exists in the database
    $this->assertDatabaseHas('users', [
        'id' => $user2->id,
    ]);
});

