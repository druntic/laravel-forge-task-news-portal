<?php

use App\Models\User;
use App\Providers\RouteServiceProvider;

test('login screen can be rendered', function () {
    $response = $this->get('/login');

    $response->assertStatus(200);
});

test('users can authenticate using the login screen', function () {
    $user = User::factory()->create();

    $response = $this->post('/login', [
        'email' => $user->email,
        'password' => 'password',
    ]);
    //throw new \Exception($response->getContent());
    $this->assertAuthenticated();
    $response->assertRedirect('/');
});

test('users can not authenticate with invalid password', function () {
    $user = User::factory()->create();

    $this->post('/login', [
        'email' => $user->email,
        'password' => 'wrong-password',
    ]);

    $this->assertGuest();
});

test('guests are redirected to login when trying to access the homepage', function () {
    // Send a GET request to the homepage while not authenticated
    $response = $this->get('/');

    // Assert that the response is a redirect
    $response->assertRedirect('/login');
});


test('users are not redirected to login when trying to acces the homepage', function () {
    // Create a user and log them in
    $user = User::factory()->create();
    $this->actingAs($user);

    // Call a protected route
    $response = $this->get('/');

    // Assert that the response is successful
    $response->assertOk();

    // Assert that the user is not redirected to the login page
    $response->assertDontSee('/login');
});

test('guests are redirected to login when trying to access the all news page', function () {
    // Send a GET request to the homepage while not authenticated
    $response = $this->get('/');

    // Assert that the response is a redirect
    $response->assertRedirect('/login');
});

test('users are not redirected to login when trying to access the all news page', function () {
    // Create a user and log them in
    $user = User::factory()->create();
    $this->actingAs($user);

    // Call a protected route
    $response = $this->get('/');

    // Assert that the response is successful
    $response->assertOk();

    // Assert that the user is not redirected to the login page
    $response->assertDontSee('/login');
});

test('guests are redirected to login when trying to access user favorites', function () {
    // Send a GET request to the homepage while not authenticated
    $response = $this->get('/users/favorites');

    // Assert that the response is a redirect
    $response->assertRedirect('/login');
});

test('users are not redirected to login when trying to access user favorites', function () {
    // Create a user and log them in
    $user = User::factory()->create();
    $this->actingAs($user);

    // Call a protected route
    $response = $this->get('/users/favorites');

    // Assert that the response is successful
    $response->assertOk();

    // Assert that the user is not redirected to the login page
    $response->assertDontSee('/login');
});
