<?php

use App\Models\Logs;
use App\Models\News;
use App\Models\User;
use App\Models\UserComment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\RefreshDatabase;

test('user can comment on news', function () {

    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Make the request with fake comment data
    $response = $this->postJson(route('comments.store'), [
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
    ]);

    // Assert the comment record is created in the database
    $this->assertDatabaseHas('user_comments_table', [
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);

    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $user->id,
        'action' => 'commented',
        'description' => "User commented on Another Fake News",
    ]);

    // Assert that only one log record is created
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response->assertSessionHasNoErrors();
});

test('guest cant comment on news', function () {

    // Make the request with fake comment data
    $response = $this->postJson(route('comments.store'), [
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
    ]);

    // Assert the comment record is not created in the database
    $this->assertDatabaseMissing('user_comments_table', [
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => null,
    ]);

    // Assert the log record is not created in the database
    $this->assertDatabaseMissing('user_logs_table', [
        'user_id' => null,
        'action' => 'commented',
        'description' => "User commented on Another Fake News",
    ]);

    // Assert that no log record is created
    $this->assertDatabaseCount('user_logs_table', 0);

});

test('user can delete comments', function () {

    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Create a fake comment record in the database
    $comment = UserComment::factory()->create([
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);

    // Make the request with the comment id
    $response = $this->deleteJson('/comments/' . $comment->id);

    // Assert the comment record is missing from the database
    $this->assertDatabaseMissing('user_comments_table', [
        'id' => $comment->id,
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);

    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $user->id,
        'action' => 'delete_comment',
        'description' => "User removed comment on Another Fake News",
    ]);

    // Assert that only one log record is created
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response->assertSessionHasNoErrors();
});


test('admin can delete user comments', function () {

    // Create a user using a model factory
    $user = User::factory()->create();
    // Create a user with is_admin set to true
    $admin = User::factory()->create(['is_admin' => true]);
    // Authenticate the user
    $this->actingAs($user);

    // Create a fake comment record in the database
    $comment = UserComment::factory()->create([
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);


    // Log in as the admin user
    $this->actingAs($admin);

    // Make the request with the comment id
    $response = $this->deleteJson(route('admin.userComments.delete', $comment->id) );

    // Assert the comment record is missing from the database
    $this->assertDatabaseMissing('user_comments_table', [
        'id' => $comment->id,
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);

    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $admin->id,
        'action' => 'delete_comment',
        'description' => "Admin removed comment on Another Fake News",
    ]);

    // Assert that only one log record is created
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response->assertSessionHasNoErrors();
});


test('user can modify comments', function () {

    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Create a fake comment record in the database
    $comment = UserComment::factory()->create([
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);

    // Make a request to update the comment with new content
    $response = $this->put(route('update.comment', $comment->id), [
        'id' => $comment->id,
        'content' => 'This is an updated comment.'
    ]);

    // Assert the comment record is updated in the database
    $this->assertDatabaseHas('user_comments_table', [
        'id' => $comment->id,
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is an updated comment.',
        'user_id' => $user->id,
    ]);

    // Assert the log record is created in the database
    $this->assertDatabaseHas('user_logs_table', [
        'user_id' => $user->id,
        'action' => 'update',
        'description' => "User modified comment on Another Fake News",
    ]);

    // Assert that only one log record is created
    $this->assertDatabaseCount('user_logs_table', 1);

    // Assert there are no validation errors in the session
    $response->assertSessionHasNoErrors();
});

test('user can only update his own comments', function () {

    // Create two users using a model factory
    $user1 = User::factory()->create();
    $user2 = User::factory()->create();

    // Authenticate the first user
    $this->actingAs($user1);

    // Create a fake comment record in the database by the first user
    $comment = UserComment::factory()->create([
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user1->id,
    ]);

    // Authenticate the second user
    $this->actingAs($user2);

    // Make a request to update the comment with new content
    $response = $this->put(route('update.comment', $comment->id), [
        'id' => $comment->id,
        'content' => 'This is an updated comment.'
    ]);

    // Assert the comment record is not updated in the database
    $this->assertDatabaseMissing('user_comments_table', [
        'id' => $comment->id,
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is an updated comment.',
        'user_id' => $user1->id,
    ]);

    // Assert the log record is not created in the database
    $this->assertDatabaseMissing('user_logs_table', [
        'user_id' => $user2->id,
        'action' => 'update',
        'description' => "User modified comment on Another Fake News",
    ]);

    // Assert that no log record is created
    $this->assertDatabaseCount('user_logs_table', 0);

});


test('admin can update users comments', function () {

    // Create a user with is_admin set to true
    $admin = User::factory()->create(['is_admin' => true]);

    // Create another user with is_admin set to false
    $user = User::factory()->create(['is_admin' => false]);

    // Create a comment by the other user
    $comment = UserComment::factory()->create(['user_id' => $user->id]);

    // Log in as the admin user
    $this->actingAs($admin);

    // Make a request to update the comment with new content
    $response = $this->put(route('admin.userComments.update', $comment->id), [
        'id' => $comment->id,
        'content' => 'New content'
    ]);
    // Assert that the response status is 302 (redirect)
    $response->assertStatus(302);

    // Assert that the session has a success message
    $response->assertSessionHas('success', 'Comment updated successfully.');

    // Assert that the comment text has been updated in the database
    $this->assertDatabaseHas('user_comments_table', [
        'id' => $comment->id,
        'text' => 'New content'
    ]);
});


test('check if users cant update their comment after 3 seconds', function () {
// Create a user and a comment using factories
$user = User::factory()->create();
    // Create a fake comment record in the database by the first user
    $comment = UserComment::factory()->create([
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment.',
        'user_id' => $user->id,
    ]);


// Use Carbon to set the updated_at timestamp of the comment to 3 seconds ago
$comment->updated_at = now()->subSeconds(5);
$comment->save();

// Make a POST request to the update comment route with some new data
$response = $this->actingAs($user)->post(route('update.comment', $comment->id), [
    'text' => 'This is an updated comment',
]);


// Assert that the comment was not updated in the database
$this->assertDatabaseMissing('user_comments_table', [
    'id' => $comment->id,
    'text' => 'This is an updated comment',
]);
});

test('check if users cant delete their comment after 3 seconds', function () {
    // Create a user and a comment using factories
    $user = User::factory()->create();
    // Create a fake comment record in the database by the first user
    $comment = UserComment::factory()->create([
        'url' => 'https://example.com/news/2',
        'title' => 'Another Fake News',
        'text' => 'This is a fake comment',
        'user_id' => $user->id,
    ]);


    // Use Carbon to set the updated_at timestamp of the comment to 3 seconds ago
    $comment->updated_at = now()->subSeconds(3);
    $comment->save();

    // Make a POST request to the update comment route with some new data
    $response = $this->actingAs($user)->post(route('delete.comment', $comment->id));


    // Assert that the comment was not deleted from the database
    $this->assertDatabaseHas('user_comments_table', [
        'id' => $comment->id,
        'text' => 'This is a fake comment',
    ]);
});
