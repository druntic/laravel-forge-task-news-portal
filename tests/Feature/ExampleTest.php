<?php

use App\Models\User;

it('returns a successful response', function () {

    // Create a user using a model factory
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    $response = $this->get('/');

    $response->assertStatus(200);
});
