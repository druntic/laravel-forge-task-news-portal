<?php
return [
    // Create the languages array
    'languages' => [
        'ar' => 'Arabic',
        'en' => 'English',
        'cn' => 'Chinese',
        'de' => 'German',
        'es' => 'Spanish',
        'fr' => 'French',
        'he' => 'Hebrew',
        'it' => 'Italian',
        'nl' => 'Dutch',
        'no' => 'Norwegian',
        'pt' => 'Portuguese',
        'ru' => 'Russian',
        'sv' => 'Swedish',
        'ud' => 'Urdu',
    ],

    'countries' => [
        'ae' => 'United Arab Emirates',
        'ar' => 'Argentina',
        'at' => 'Austria',
        'au' => 'Australia',
        'be' => 'Belgium',
        'bg' => 'Bulgaria',
        'br' => 'Brazil',
        'ca' => 'Canada',
        'ch' => 'Switzerland',
        'cn' => 'China',
        'co' => 'Colombia',
        'cu' => 'Cuba',
        'cz' => 'Czech Republic',
        'de' => 'Germany',
        'eg' => 'Egypt',
        'fr' => 'France',
        'gb' => 'United Kingdom',
        'gr' => 'Greece',
        'hk' => 'Hong Kong',
        'hu' => 'Hungary',
        'id' => 'Indonesia',
        'ie' => 'Ireland',
        'il' => 'Israel',
        'in' => 'India',
        'it' => 'Italy',
        'jp' => 'Japan',
        'kr' => 'South Korea',
        'lt' => 'Lithuania',
        'lv' => 'Latvia',
        'ma' => 'Morocco',
        'mx' => 'Mexico',
        'my' => 'Malaysia',
        'ng' => 'Nigeria',
        'nl' => 'Netherlands',
        'no' => 'Norway',
        'ph' => 'Philippines',
        'pl' => 'Poland',
        'pt' => 'Portugal',
        'ro' => 'Romania',
        'rs' => 'Serbia',
        'ru' => 'Russia',
        'sa' => 'Saudi Arabia',
        'se' => 'Sweden',
        'sg' => 'Singapore',
        'si' => 'Slovenia',
        'sk' => 'Slovakia',
        'th' => 'Thailand',
        'tr' => 'Turkey',
        'tw' => 'Taiwan',
        'ua' => 'Ukraine',
        'us' => 'United States',
        've' => 'Venezuela',
        'za' => 'South Africa'
        // Add more countries here
    ],

    'categories' => [
        'business',
        'entertainment',
        'general',
        'health',
        'science',
        'sports',
        'technology',
    ],
    // Create the languages array
    'reverseLanguages' => [
        'Arabic' => 'ar',
        'English' => 'en',
        'Chinese' => 'cn',
        'German' => 'de',
        'Spanish' => 'es',
        'French' => 'fr',
        'Hebrew' => 'he',
        'Italian' => 'it',
        'Dutch' => 'nl',
        'Norwegian' => 'no',
        'Portuguese' => 'pt',
        'Russian' => 'ru',
        'Swedish' => 'sv',
        'Urdu' => 'ud',
    ]

];
