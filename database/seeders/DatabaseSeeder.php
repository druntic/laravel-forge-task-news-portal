<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Create an admin user with predefined credentials
        User::create([
            'id' => Str::uuid(),
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
            'is_admin' => true,
            'language' => 'en',
            'country' => 'us',
            'favoriteCategory' => 'general'
        ]);

        // Create an normal user with predefined credentials
        User::create([
            'id' => Str::uuid(),
            'name' => 'Matej',
            'email' => 'matej@test.com',
            'password' => Hash::make('12345678'),
            'is_admin' => false,
            'language' => 'en',
            'country' => 'us',
            'favoriteCategory' => 'general'
        ]);

        // Create an normal user with predefined credentials
        User::create([
            'id' => Str::uuid(),
            'name' => 'Karlo',
            'email' => 'karlo@test.com',
            'password' => Hash::make('12345678'),
            'is_admin' => false,
            'language' => 'en',
            'country' => 'us',
            'favoriteCategory' => 'general'
        ]);
    }
}
