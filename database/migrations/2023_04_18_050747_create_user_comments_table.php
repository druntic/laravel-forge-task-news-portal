<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_comments_table', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('url')->comment('The URL of the comment');
            $table->string('title')->comment('The title of the comment');
            $table->text('text')->comment('The text of the comment');
            $table->timestamps();
            // Add a foreign key constraint on user_id column
            $table->foreignUuid('user_id')->constrained()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_comments_table');
    }
};
