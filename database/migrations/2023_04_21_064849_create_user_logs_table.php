<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_logs_table', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('action', ['favorited', 'commented', 'update', 'delete_comment', 'delete_favorite'])->comment('action the user has performed');
            $table->text('description');
            $table->foreignUuid('user_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_logs_table');
    }
};
