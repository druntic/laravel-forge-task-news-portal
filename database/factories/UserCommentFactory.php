<?php

namespace Database\Factories;

use App\Models\UserComment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserCommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserComment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'url' => $this->faker->url,
            'title' => $this->faker->sentence,
            'text' => $this->faker->paragraph,
            'user_id' => $this->faker->randomDigitNotNull,
        ];
    }
}

