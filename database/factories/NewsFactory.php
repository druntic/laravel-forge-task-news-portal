<?php

namespace Database\Factories;

use App\Models\News;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class NewsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = News::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'url' => $this->faker->url,
            'title' => $this->faker->sentence,
            'image' => $this->faker->imageUrl,
            'description' => $this->faker->paragraph,
            'user_id' => $this->faker->randomDigitNotNull,
        ];
    }
}
