##News web app

## Description:

This is a news website built with Laravel framework and PostgreSQL database.

There are three main roles that can use this app:

### Guest users:
- can access register and login page
### Registered users:
- view all news
-  add and remove news to favorite news
- add, edit or remove users comments
- change the language, country or favorite category
### Administrator:
- can modify the comments of all users
- can modify the favorite news of all users
- can view the history of all users actions
- can view a list of all users


## Installation And Running The App

Follow these steps to install and run the project:

1. Install Homebrew using this command:
```
bash/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
2. Update Homebrew using this command:
```brew update```

3. Install PHP using this command:
```brew install php```

4. Install Composer using this command:
```brew install composer```
5. Check if Composer is in your PATH by running this command:
```echo $PATH```
If you don’t see “composer” anywhere, run this command:

```code ~/.zshrc```
And add this line to the file:

```export PATH=$PATH:$HOME/.composer/vendor/bin```

6. Restart your terminal and go to your project folder.

7. Install the News api with:
```composer require jcobhams/newsapi```

8. Run the project using this command:
```php artisan serve```
This will start the laravel project
```Laravel development server started: http://127.0.0.1:8000```
Open your browser and go to http://127.0.0.1:8000 to see the website.
### Migrations
To create all the nessesary tables and columns, run the following
```
php artisan migrate
```

### Seeding The Database
To add the dummy users
```
php artisan db:seed
```
Credentials for dummy users:

Admin: 
- email: admin@admin.com
- pass: admin

User1:
- email: karlo@test.com
- pass: 12345678

User2:
- email: matej@test.com
- pass: 12345678

### Testing
To run all the tests, run the following
```
php artisan test
```
