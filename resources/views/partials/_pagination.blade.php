    {{-- pagination --}}
    <p style="text-align: center;">
        @for ($i = 1; $i <= $pageCount; $i++)
            @if ($i == $page)
                <button class="active-pagination-btn"
                    style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; border-radius: 5px; background-color: white; margin: 5px; color: blue; font-weight: bold;">
                    {{ $i }}
                </button>
            @elseif ($i == 1)
                <a href="{{ $uri }}?page={{ $i }}&search={{ $search }}&sort={{ $sort }}&category={{ $category }}&source={{ $source }}&from={{ $from }}&to={{ $to }}&pageSize={{ $pageSize }}"
                    style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; border-radius: 5px; background-color: white; margin: 5px;">
                    {{ $i }}
                </a>
            @elseif ($i == 2)
                @if ($page == 1 || $page == 3)
                    <a href="{{ $uri }}?page={{ $i }}&search={{ $search }}&sort={{ $sort }}&category={{ $category }}&source={{ $source }}&from={{ $from }}&to={{ $to }}&pageSize={{ $pageSize }}"
                        style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; border-radius: 5px; background-color: white; margin: 5px;">
                        {{ $i }}
                    </a>
                    @if ($page == 1)
                        ...
                    @endif
                @endif
                @if ($page > 3)
                    ...
                @endif
            @elseif ($i == $page - 1)
                <a href="{{ $uri }}?page={{ $i }}&search={{ $search }}&sort={{ $sort }}&category={{ $category }}&source={{ $source }}&from={{ $from }}&to={{ $to }}&pageSize={{ $pageSize }}"
                    style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; border-radius: 5px; background-color: white; margin: 5px;">
                    {{ $i }}
                </a>
            @elseif ($i == $page + 1)
                <a href="{{ $uri }}?page={{ $i }}&search={{ $search }}&sort={{ $sort }}&category={{ $category }}&source={{ $source }}&from={{ $from }}&to={{ $to }}&pageSize={{ $pageSize }}"
                    style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; border-radius: 5px; background-color: white; margin: 5px;">
                    {{ $i }}
                </a>
                @if ($i < $pageCount - 1)
                    ...
                @endif
            @elseif ($i == $pageCount)
                <a href="{{ $uri }}?page={{ $i }}&search={{ $search }}&sort={{ $sort }}&category={{ $category }}&source={{ $source }}&from={{ $from }}&to={{ $to }}&pageSize={{ $pageSize }}"
                    style="display: inline-block; width: 30px; height: 30px; line-height: 30px; text-align: center; border-radius: 5px; background-color: white; margin: 5px;">
                    {{ $i }}</a>
            @endif
        @endfor
    </p>
