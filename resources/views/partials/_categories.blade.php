<div style="background-color: white; border-radius: 10px; padding: 10px;">

    <div id="table-container" style="text-align: center;">

      <!-- A table element with some a tags -->
      <table border="1" style="color: blue; display: inline-block; font-size: 18px;">
        <tr>
          @foreach ($categories as $cat)
            <td style="padding: 5px;">
              <a href="{{$uri}}?&search={{ $search}}&category={{ $cat }}"
                 style="{{ $cat == $category ? 'color: red;' : '' }}">
                {{ ucfirst($cat) }}
              </a>
            </td>
          @endforeach
        </tr>
      </table>

    </div>

  </div>
