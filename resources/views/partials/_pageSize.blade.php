<style>
    .rounded-div {
        background-color: white;
        border-radius: 10px;
        padding: 20px;
        margin: 10px;
        width: fit-content;
    }
</style>
<div class="rounded-div">
    <form action="{{ url()->current() }}" method="GET">
        @csrf
        <label for="pageSize">Select page size:</label>
        <select name="pageSize" id="pageSize">
            <option value="1" {{ $pageSize == 1 ? 'selected' : '' }}>1</option> <!-- This will compare the option value with the variable $page_size -->
            <option value="2" {{ $pageSize == 2 ? 'selected' : '' }}>2</option>
            <option value="5" {{ $pageSize == 5 ? 'selected' : '' }}>5</option>
            <option value="10" {{ $pageSize == 10 ? 'selected' : '' }}>10</option>
        </select>
        <button type="submit">Submit</button>
    </form>
</div>
