<!-- A button to toggle the drop down table with a style attribute -->
<button onclick="toggleTable()" style="color: blue; background-color: white; border-radius: 10px; padding: 5px; border: 1px solid blue; box-shadow: 2px 2px 5px gray; font-family: Arial;">Filter</button>
<div id="table-container2" style="background-color: white; border-radius: 10px; padding: 10px; display: none">
    <!-- A form element with a method and an action attribute -->
    <form method="GET" action="{{$uri}}">
        <!-- A for loop to create a checkbox element for each name -->
        @for ($i = 0; $i < count($name); $i++)
            <!-- A label element for the checkbox -->
            <label for="{{ $sourceId[$i] }}">{{ $name[$i] }}</label>
            <!-- A checkbox element with a name and a value attribute -->
            <input type="checkbox" id="{{ $sourceId[$i] }}" name="source[]" value="{{ $sourceId[$i] }}"
            @if (in_array($sourceId[$i], $sourceArray)) checked @endif>
        @endfor
            <!-- A submit button -->
    <input type="submit" value="Submit">
    <!-- A cancel button -->
    <input type="button" value="Cancel" onclick="toggleTable()">
    </form>

</div>


<!-- A script element to define the toggleTable function -->
<script>
  // A function to show or hide the table-container element
  function toggleTable() {
    // Get the table-container element by its id
    var tableContainer = document.getElementById("table-container2");

    // Check the current display style of the table-container element
    if (tableContainer.style.display == "none") {
      // If it is none, change it to block
      tableContainer.style.display = "block";
    } else {
      // If it is not none, change it to none
      tableContainer.style.display = "none";
    }
  }
</script>
