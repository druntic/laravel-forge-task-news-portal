<form method="POST" action="{{ route('password.email') }}">
    @csrf
    <input type="hidden" id="email" class="block mt-1 w-full" type="email" name="email" :value="{{$user->email}}">
    <x-danger-button>
        {{ __('Reset Account password') }}
    </x-danger-button>
</form>
