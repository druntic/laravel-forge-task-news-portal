<x-app-layout>
    <script>
        // Define a function that will toggle the display of the user names div
        function toggleUserDiv() {
          // Get the user names div by its id
          var userDiv = document.getElementById("userDiv");
          // Check if the div is hidden or not
          if (userDiv.hidden) {
            // If hidden, show it
            userDiv.hidden = false;
          } else {
            // If not hidden, hide it
            userDiv.hidden = true;
          }
        }
      </script>
      <style>
        /* Define a custom class for the divs */
        .user-div {
          /* Add some margin to the bottom of each div */
          margin-bottom: 1rem;
        }
      </style>

      <button type="button" onclick="toggleUserDiv()" style="color: blue; background-color: white; border-radius: 10px; padding: 5px; border: 1px solid blue; box-shadow: 2px 2px 5px gray; font-family: Arial; display: block; margin: 0 auto;">
          View all users
      </button>
    <!-- The div that will be hidden by default and shown when the button is clicked -->
    <div id="userDiv" hidden style=> <!-- A wrapper div with a white background, rounded corners and centered content -->
        <!-- Add a search input field and a search button -->
        <input type="text" id="searchInput" class="form-control" placeholder="Search by User ID or Name">
        <button type="button" onclick="searchUsers()" class="btn btn-primary" style="color: blue; background-color: white; border-radius: 10px; padding: 5px; border: 1px solid blue; box-shadow: 2px 2px 5px
         gray; font-family: Arial;">Search</button>
        @foreach ($userNames as $index => $userName)
        <div class="bg-white rounded p-3 text-center user-div" data-id="{{ $userIds[$index] }}" data-name="{{ $userName }}">
            <!-- Use flexbox to create a row with two columns -->
            <div style="display: flex; justify-content: space-between; align-items: center;">
                <!-- The first column for the name and id -->
                <div>
                    <!-- Display the loop iteration before the name -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#userModal" data-name="{{ $userName }}">
                        {{ $loop->iteration }}. {{ $userName }}
                    </button>
                    <!-- Display the id after the name -->
                    <!-- Click on user id to go to user info -->
                    <a href="{{ route('admin.userInfo', $userIds[$index]) }}" class="btn btn-primary">ID: {{ $userIds[$index] }}</a>

                </div>
                <!-- The second column for the buttons -->
                <div>
                    <!-- Add a Logs button that links to the userLogs route -->
                    <a href="{{ route('admin.userLogs', $userIds[$index]) }}" class="btn btn-primary">Logs</a>
                    <!-- Add a Favorites button that links to the userFavorites route -->
                    <a href="{{ route('admin.userFavorites', $userIds[$index]) }}" class="btn btn-success">Favorites</a>
                    <!-- Add a Comments button that links to the userComments route -->
                    <a href="{{ route('admin.userComments', $userIds[$index]) }}" class="btn btn-secondary">Comments</a>
                </div>
            </div>
        </div>
    @endforeach






    </div>

</x-app-layout>

<script>
    function searchUsers() {
    // Get the search query from the input field
    var searchQuery = document.getElementById('searchInput').value.toLowerCase();

    // Get all the user divs
    var userDivs = document.getElementsByClassName('user-div');

    // Loop through each user div and hide/show based on search query
    for (var i = 0; i < userDivs.length; i++) {
        var userName = userDivs[i].getAttribute('data-name').toLowerCase();
        var userId = userDivs[i].getAttribute('data-id').toLowerCase();

        // Show the user div if the search query matches the user ID or user name
        if (userName.includes(searchQuery) || userId.includes(searchQuery)) {
            userDivs[i].style.display = 'block';
        } else {
            userDivs[i].style.display = 'none';
        }
    }
}
</script>
