<x-app-layout>
    <!-- Create a div element with a style attribute -->
    <div style="background-color: white; border-radius: 10px; padding: 10px;">
        <!-- Place your table inside the div -->
        <!-- Add an id attribute to your table -->
        <table id="log-table">
            <!-- Add a thead element to your table -->
            <thead>
                <!-- Move the table header row inside the thead element -->
                <tr>
                    <!-- Create table header cells for each column name with a style attribute -->
                    <!-- Add a class attribute to your header cells -->
                    <th class="header" style="text-align: center; padding: 5px;">Date</th>
                    <th class="header" style="text-align: center; padding: 5px;">Action</th>
                    <th style="text-align: center; padding: 5px;">Description</th>
                </tr>
            </thead>
            <!-- Add a tbody element to your table -->
            <tbody>
                <!-- Loop through the logs collection and create a table row for each log -->
                @foreach ($date as $i => $value)
                <tr>
                    <!-- Create table data cells for each log attribute -->
                    <!-- Format the date as YYYY-MM-DD -->
                    <td style="padding: 5px;">{{ date('Y-m-d H:i:s', strtotime($date[$i])) }}</td>
                    <td style="padding: 5px;">{{ $action[$i] }}</td>
                    <td style="padding: 5px;">{{ $description[$i] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </x-app-layout>

    <!-- Add a script tag at the end of your view -->
    <script>
    // Get the table element by its id
    var table = document.getElementById("log-table");
    // Get all the header cells by their class name
    var headers = document.getElementsByClassName("header");
    console.log(headers);
    // Loop through the header cells
    for (var i = 0; i < headers.length; i++) {
        // Add a click event listener to each header cell
        headers[i].addEventListener("click", function() {
            // Get the index of the clicked header cell
            var index = this.cellIndex;
            // Get the direction of the sorting (ascending or descending)
            var direction = this.getAttribute("data-direction") || "asc";
            // Toggle the direction for the next click
            direction = direction === "asc" ? "desc" : "asc";
            // Set the direction as an attribute of the header cell
            this.setAttribute("data-direction", direction);
            // Sort the table rows by the index and direction
            sortTable(table, index, direction);
        });
    }

    // Define a function to sort the table rows by a given index and direction
    function sortTable(table, index, direction) {
        // Get all the table rows except the header row from the tbody element
        var rows = Array.from(table.tBodies[0].rows);
        // Sort the rows by comparing the text content of the cells at the given index
        rows.sort(function(a, b) {
            var x = a.cells[index].textContent;
            var y = b.cells[index].textContent;
            // If the index is 0 (the date column), compare x and y as Date objects instead of strings
            if (index === 0) {
                x = new Date(x);
                y = new Date(y);
            }
            // If the direction is ascending, return -1 if x is less than y, 1 if x is greater than y, or 0 if equal
            if (direction === "asc") {
                return x < y ? -1 : x > y ? 1 : 0;
            }
            // If the direction is descending, return -1 if x is greater than y, 1 if x is less than y, or 0 if equal
            else {
                return x > y ? -1 : x < y ? 1 : 0;
            }
        });
        // Remove all the rows from the table body
        table.tBodies[0].innerHTML = "";
        // Append the sorted rows to the table body
        for (var row of rows) {
            table.tBodies[0].appendChild(row);
        }
    }
    </script>
