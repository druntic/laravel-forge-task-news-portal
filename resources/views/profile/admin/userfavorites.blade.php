<x-app-layout>
    <head>
        <!-- ... -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
      </head>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

      <div style="display: flex; flex-wrap: wrap; justify-content: center;">
        @for ($i = 0; $i < count($titles); $i++)
        <div style="background-color: white; border-radius: 10px; margin: 10px; width: 400px; text-align: center; position: relative;">
          <a href="{{ $urls[$i] }}" target="blank">
            <img src="{{ $images[$i]}}" alt="News image" style="width: 100%; height: auto;">
          </a>
          <p style="">{{ $titles[$i] }} <i>{{ $dates[$i] }}</i></p>
                <!-- Add a form with a hidden input field and a submit button here -->
                <form method="POST" action="{{ route('admin.userFavorites.delete', $id) }}" class="delete-form">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <input type="hidden" name="id" value="{{ $newsIds[$i] }}">
                  <button type="submit" class="delete-button"><i class="fa fa-times"></i></button>
                </form>
        </div>
        @endfor
    </div>
</x-app-layout>
