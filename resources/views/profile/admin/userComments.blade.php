<x-app-layout>
    <head>
        <!-- ... -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <!-- Add the styles here -->
        <style>
            .comments {
                margin: 20px;
                padding: 10px;
                border-radius: 10px;
                background-color: white;
            }

            .comments h3 {
                text-align: center;
            }

            .comment {
                max-width: 500px; /* adjust this value as you like */
                margin: 10px;
                padding: 5px;
                border: 1px solid gray;
                word-break: break-word; /* or overflow-wrap: break-word; */
            }

            /* Hide all comments except the first four */
            .comment:nth-child(n+4) {
                display: none;
            }

            /* Show a button to expand the comments */
            .show-more {
                display: block;
                margin: 10px;
                padding: 5px;
                border: 1px solid gray;
                cursor: pointer;
            }

            /* Hide the button when all comments are shown */
            .show-more.expanded {
                display: none;
            }

        </style>
      </head>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <div style="display: flex; flex-wrap: wrap; justify-content: center;">
        <div style="border-radius: 10px; background-color: white;">


            <!-- Wrap the comments section with a div with the class comments -->
            <div class="comments">
                <!-- Add a heading element with the text Comments -->
                <h3>Comments by {{$name}}</h3>


                <!-- Loop through the comments using the foreach blade directive -->
                @foreach ($comments as $index => $t)
                    <!-- Display the text, user name and date of each comment -->
                    <div class="comment">
                        <p>Title: {{$titles[$index]}}</p>
                        <p>Comment: {{ $t }}</p>
                        <p> Commented on: {{ $dates[$index] }}</p>
                        <!-- Check if the user is authenticated and their id matches the comment user id -->
                        @auth
                            @if (auth()->user()->is_admin)
                                <!-- Display buttons for editing and deleting their own comments -->
                                <div class="container d-flex">
                                <!-- Use a data attribute to store the comment id -->
                                <button data-id="{{ $comment_ids[$index] }}">Edit</button>
                                <!-- Use a form with a hidden input field and a textarea field for editing -->
                                <form id="edit-form-{{ $comment_ids[$index] }}" action="{{ route('admin.userComments.update', $comment_ids[$index]) }}" method="POST" style="display: none;">
                                    @csrf
                                    {{ method_field('PUT') }}
                                    <input type="hidden" name="id" value="{{ $comment_ids[$index] }}">
                                    <textarea name="content" style="resize: none;">{{ old('content', $t) }}</textarea>
                                    <button type="submit">Submit</button>
                                </form>

                                    <!--deleting comment-->
                                    <form action="{{ route('admin.userComments.delete', $comment_ids[$index]) }}" method="POST">
                                      @csrf
                                      <input type="hidden" name="_method" value="DELETE">
                                      <button type="submit" class="delete-button">Delete</button>
                                    </form>
                                  </div>
                            @endif
                        @endauth
                    </div>
                @endforeach
            </div>
            <!-- Add a button element with the class show-more -->
            <button class="show-more">Show more</button>

        </div>
    </div>
</x-app-layout>



<!-- Add the script here -->
<script>
// Get the show more button element
var showMore = document.querySelector(".show-more");
//hide the showbutton if there are no comments
if (document.querySelectorAll(".comment:nth-child(n+4)").length == 0){
    // Hide the button
    showMore.style.display = "none";
}
// Initialize a variable to keep track of how many comments are shown
var shownComments = 2;

// Add a click event listener to the button
showMore.addEventListener("click", function() {
    //console.log(shownComments);
    // Get all the hidden comments
    var hiddenComments = document.querySelectorAll(".comment:nth-child(n+4)");
    console.log(hiddenComments)
    // Get the next 2 hidden comments using the slice method
    //this function make gets the subarray of the array hiddenComments from indices shownComments-2 to shownComments
    //excluding the last index number
    //eg. (array, 1,3) will return the array from index 1 to index 2
    var nextComments = Array.prototype.slice.call(hiddenComments, shownComments - 2, shownComments - 0);
    // Loop through the next comments and show them
    //console.log('next Comments', nextComments);
    for (var i = 0; i < nextComments.length; i++) {
        nextComments[i].style.display = "block";
    }
    // Update the shown comments variable
    shownComments += nextComments.length;
    // Check if there are no more hidden comments
    if (shownComments >= document.querySelectorAll(".comment").length) {
        // Hide the button
        showMore.style.display = "none";
    }
});

    // Get all the edit buttons
    const editButtons = document.querySelectorAll('button[data-id]');

    // Loop through each button and add an event listener
    editButtons.forEach(button => {
        button.addEventListener('click', function() {
            // Get the comment id from the data attribute
            const commentId = this.dataset.id;
            // Find the corresponding form element using a query selector
            const form = document.getElementById("edit-form-" + commentId);
            // Toggle the display style of the form element
            form.style.display = form.style.display === 'none' ? 'block' : 'none';
        });
    });


// Get all the form elements
var forms = document.querySelectorAll('form');

// Loop through each form element
forms.forEach(function(form) {
  // Add a submit event listener to the form
  form.addEventListener('submit', function(event) {
    // Get the submit button of the form
    var submitButton = form.querySelector('[type="submit"]');
    // Disable the submit button
    submitButton.setAttribute('disabled', 'disabled');
    // The rest of your code
    // ...
    // Set a timeout to enable the submit button after 2 seconds
    setTimeout(function() {
      submitButton.removeAttribute('disabled');
    }, 2000);
  });
});
    </script>

