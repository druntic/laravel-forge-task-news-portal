<x-app-layout>


@include('partials._search')
@include('partials._categories')
@include('partials._filterTop')


<head>
    <!-- ... -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
  </head>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

  <div style="display: flex; flex-wrap: wrap; justify-content: center;">
    @for ($i = 0; $i < count($news); $i++)
    <div style="background-color: white; border-radius: 10px; margin: 10px; width: 400px; text-align: center;">
      <a href="{{ $news[$i]->url }}" target="blank">
        <img src="{{ $news[$i]->urlToImage }}" alt="News image" style="width: 100%; height: auto;">
      </a>
      <p style="">{{ $news[$i]->title }} <i>{{ $news[$i]->publishedAt }}</i></p>
      <!-- Add the icon here -->
      <form method="post" action="{{ route('save.news') }}">
        @csrf
        <input type="hidden" name="news_url" value="{{ $news[$i]->url }}">
        <input type="hidden" name="news_image" value="{{ $news[$i]->urlToImage }}">
        <input type="hidden" name="news_title" value="{{ $news[$i]->title }}">
        <input type="hidden" name="news_date" value="{{ $news[$i]->publishedAt }}">
        <input type="hidden" name="news_description" value="{{ $news[$i]->description }}">
        <input type="hidden" name="user_id" value="{{ auth()->id() }}">
        <button type="submit" style="border: none; background: none; margin-right: 30px;">
        <i class="far fa-heart"></i>
        </button>

        <!-- encode the link so it doesnt cause issues with the routes-->
        <button type="button" style="border: none; background: none; margin-left: 30px;"
        onclick="window.location.href = '{{ route('news.show', ['url' => bin2hex($news[$i]->url)
        , 'title' => $news[$i]->title, 'country' => $country, 'category' => $category, 'isTop' => 1]) }}'">
            {{$comments[$i]}} Comments
        </button>

</form>
    </div>
    @endfor
</div>


<script>
    //script for toggle heart icon
    // Select all the heart icons
    var hearts = $(".fa-heart");

    // Add a click event listener to each icon
    hearts.click(function() {
      // Toggle the class names to switch between hollow and full icons
      $(this).toggleClass("far fas");
    });
  </script>

@include('partials._pagination')
@include('partials._pageSize')
</x-app-layout>
