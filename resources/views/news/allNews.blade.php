<x-app-layout>


    @include('partials._search')
    @include('partials._filterEverything')


    <head>
        <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
        <!--Get your code at fontawesome.com-->
        <style>
            /* Style the input box */
            .comment-box {
                display: none;
                /* Hide by default */
                position: relative;
                /* Position it relative to the parent element */
                top: 10px;
                /* Set some distance from the top */
                left: 10px;
                /* Set some distance from the left */
                transform: none;
                /* Remove the transform property */
                width: 300px;
                /* Set a fixed width */
                height: auto;
                /* Adjust the height according to the content */
                border: 1px solid black;
                /* Add a border */
                padding: 10px;
                /* Add some padding */
                background-color: white;
                /* Set a background color */
                z-index: 0;
                /* Remove it from front */
                overflow: visible;
                /* Show the content without scroll bars */
            }

            /* Style the close button */
            .close {
                position: absolute;
                /* Position it relative to the parent element */
                top: 5px;
                /* Set some distance from the top */
                right: 5px;
                /* Set some distance from the right */
                color: black;
                /* Set the color */
                cursor: pointer;
                /* Change the cursor on hover */
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
    </head>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <div style="display: flex; flex-wrap: wrap; justify-content: center;">
        @for ($i = 0; $i < count($news); $i++)
            <div style="background-color: white; border-radius: 10px; margin: 10px; width: 400px; text-align: center;">
                <a href="{{ $news[$i]->url }}" target="blank">
                    <img src="{{ $news[$i]->urlToImage }}" alt="News image" style="width: 100%; height: auto;">
                </a>
                <p style="">{{ $news[$i]->title }} <i>{{ $news[$i]->publishedAt }}</i></p>
                <!-- Add a form with a hidden input field and a submit button here -->
                <form method="post" action="{{ route('save.news') }}">
                    @csrf
                    <input type="hidden" name="news_url" value="{{ $news[$i]->url }}">
                    <input type="hidden" name="news_image" value="{{ $news[$i]->urlToImage }}">
                    <input type="hidden" name="news_title" value="{{ $news[$i]->title }}">
                    <input type="hidden" name="news_date" value="{{ $news[$i]->publishedAt }}">
                    <input type="hidden" name="news_description" value="{{ $news[$i]->description }}">
                    <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                    <button type="submit" style="border: none; background: none; margin-right: 30px;">
                        <i class="far fa-heart"></i>
                    </button>
                    <!-- Add a comment icon here -->
                    <!-- encode the link so it doesnt cause issues with the routes-->
                    <button type="button" style="border: none; background: none; margin-left: 30px;"
                        onclick="window.location.href = '{{ route('news.show', ['url' => bin2hex($news[$i]->url), 'title' => $news[$i]->title]) }}'">
                        {{ $comments[$i] }} Comments
                    </button>
                </form>

            </div>
        @endfor
    </div>
    <script>
        // Select all the heart icons
        var hearts = $(".fa-heart");

        // Add a click event listener to each icon
        hearts.click(function() {
            // Toggle the class names to switch between hollow and full icons
            $(this).toggleClass("far fas");
        });
    </script>


    @include('partials._pageSize')
    @include('partials._pagination')
</x-app-layout>
