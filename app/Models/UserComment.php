<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use App\Traits\UUID;
/** @OA\Schema(
    *   schema="UserComment",
    *   type="object",
    *   required={"url", "title", "text", "user_id"},
    *   @OA\Property(
    *     property="id",
    *     type="string",
    *     format="uuid",
    *     readOnly=true,
    *     description="The unique identifier of the comment.",
    *     example="453e4567-e89b-12d3-a456-426614174000"
    *   ),
    *   @OA\Property(
    *     property="url",
    *     type="string",
    *     format="uri",
    *     description="The URL of the comment.",
    *     example="www.somewhere.com"
    *   ),
    *   @OA\Property(
    *     property="title",
    *     type="string",
    *     description="The title of the comment.",
    *     example="trump got blasted into space"
    *   ),
    *   @OA\Property(
    *     property="text",
    *     type="string",
    *     description="The text of the comment.",
    *     example="tesla launched trump into space"
    *   ),
    *   @OA\Property(
    *     property="user_id",
    *     type="string",
    *     format="uuid",
    *     readOnly=true,
    *     description="The unique identifier of the user.",
    *     example="123e4567-e89b-12d3-a456-426614174000"
    *   ),
    * )
 */
class UserComment extends Model
{

    use HasFactory,UUID;

    // The attributes that are mass assignable
    protected $fillable = [
        'url',
        'title',
        'text',
        'user_id'
    ];

    // The table associated with the model
    protected $table = 'user_comments_table';

    // The user that owns the comment
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
