<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use App\Traits\UUID;

/** @OA\Schema(
    *   schema="User",
    *   type="object",
    *   required={"name", "country", "language", "email", "password", "favouriteCategory"},
    *   @OA\Property(
    *     property="id",
    *     type="string",
    *     format="uuid",
    *     readOnly=true,
    *     description="The unique identifier of the user.",
    *     example="123e4567-e89b-12d3-a456-426614174000"
    *   ),
    *   @OA\Property(
    *     property="name",
    *     type="string",
    *     description="The name of the user.",
    *     example="pero"
    *   ),
    *   @OA\Property(
    *     property="country",
    *     type="string",
    *     description="The country of the user.",
    *     example="russia"
    *   ),
    *   @OA\Property(
    *     property="language",
    *     type="string",
    *     description="The language of the user.",
    *     example="russian"
    *   ),
    *   @OA\Property(
    *     property="email",
    *     type="string",
    *     format="email",
    *     description="The email address of the user.",
    *     example="pero@russia.ru"
    *   ),
    *   @OA\Property(
    *     property="password",
    *     type="string",
    *     format="password",
    *     writeOnly=true,
    *     description="The password of the user.",
    *     example="putPutinInPooPoo"
    *   ),
    *   @OA\Property(
    *     property="favouriteCategory",
    *     type="string",
    *     description="The favourite category of the user.",
    *     example="sports"
    *   ),
    *   @OA\Property(
    *     property="email_verified_at",
    *     type="string",
    *     format="date-time",
    *     nullable=true,
    *     readOnly=true,
    *     description="The date and time when the email address of the user was verified."
    *   ),
    * )
    */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,UUID;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'country',
        'language',
        'favoriteCategory'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function updateLocale($language)
    {
        // Create the languages array
        $languages = config('constants.reverseLanguage');
        // Set the user's preferred locale based on their language
        if (in_array($language, $languages)) {
            // Use the language code as the preferred locale
            $this->preferred_locale = $languages[$language];
        } else {
            // Use a default locale
            $this->preferred_locale = 'en';
        }
    }

    // Define the news relationship on the User model
    public function news_favorites()
    {
        return $this->hasMany(News::class);
    }

    // The comments that belong to the user
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    // The logs that belong to the user
    public function logs()
    {
        return $this->hasMany(Logs::class);
    }

    public function isAdmin()
    {
        return $this->is_admin;
    }
}
