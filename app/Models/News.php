<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use App\Traits\UUID;
/** @OA\Schema(
    *   schema="News",
    *   type="object",
    *   required={"url", "title", "image", "description", "user_id"},
    *   @OA\Property(
    *     property="id",
    *     type="string",
    *     format="uuid",
    *     readOnly=true,
    *     description="The unique identifier of the news item.",
    *     example="123e4567-e89b-12d3-a456-426614174000"
    *   ),
    *   @OA\Property(
    *     property="url",
    *     type="string",
    *     format="uri",
    *     description="The URL of the news item.",
    *     example="www.news.com"
    *   ),
    *   @OA\Property(
    *     property="title",
    *     type="string",
    *     description="The title of the news item.",
    *     example="some news"
    *   ),
    *   @OA\Property(
    *     property="image",
    *     type="string",
    *     format="uri",
    *     description="The image URL of the news item.",
    *     example="www.newsimg.com"
    *   ),
    *   @OA\Property(
    *     property="description",
    *     type="string",
    *     description="The description of the news item.",
    *     example="this is some news"
    *   ),
    *   @OA\Property(
    *     property="user_id",
    *     type="string",
    *     format="uuid",
    *     readOnly=true,
    *     description="The unique identifier of the user .",
    *     example="456e4567-e89b-12d3-a456-426614174000"
    *   ),
    * )
 */
class News extends Model
{

    use HasFactory,UUID;
    protected $fillable = [
        'url',
        'title',
        'image',
        'description',
        'user_id'
    ];
    // The table associated with the model
    protected $table = 'user_favorites_table';


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(UserComment::class);
    }
}
