<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use App\Traits\UUID;
/** @OA\Schema(
    *   schema="UserLog",
    *   type="object",
    *   required={"action", "description", "user_id"},
    *   @OA\Property(
    *     property="id",
    *     type="string",
    *     format="uuid",
    *     readOnly=true,
    *     description="The unique identifier of the user log.",
    *     example="123e4567-e89b-12d3-a456-426614174000"
    *   ),
    *   @OA\Property(
    *     property="action",
    *     type="string",
    *     description="The action performed by the user.",
    *     example="favorited"
    *   ),
    *   @OA\Property(
    *     property="description",
    *     type="string",
    *     description="The description of the action.",
    *     example="The user favorited something"
    *   ),
    *   @OA\Property(
    *     property="user_id",
    *     type="string",
    *     format="uuid",
    *     readOnly=true,
    *     description="The unique identifier of the user.",
    *     example="123e4567-e89b-12d3-a456-426614174222"
    *   ),
    * )
 */
class UserLog extends Model
{

    use HasFactory, UUID;

    protected $fillable = [
        'action',
        'description',
        'user_id'
    ];

    protected $table = 'user_logs_table';
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
