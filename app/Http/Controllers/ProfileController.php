<?php

namespace App\Http\Controllers;

use Spatie\Url\Url;
use App\Models\User;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ProfileUpdateRequest;
use Exception;
use Illuminate\Routing\Controller;

class ProfileController extends Controller
{
    /**
     * @OA\Get(
     *      path="/profile/edit",
     *      operationId="editUserProfile",
     *      tags={"Profile"},
     *      summary="Display the user's profile form",
     *      description="Returns a view with user and settings data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      )
     *     )
     */
    public function edit(Request $request): View{

        // Create the languages array
        $languages = config('constants.languages');
        $countries = config('constants.countries');
        $categories = config('constants.categories');
        return view('profile.edit', [
            'user' => $request->user(),
            'languages' => $languages,
            'countries' => $countries,
            'categories' => $categories
        ]);



    }

    /**
     * @OA\Patch(
     *      path="/profile/update",
     *      operationId="updateUserProfile",
     *      tags={"Profile"},
     *      summary="Update the user's profile information",
     *      description="Returns a redirect response with status message",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"email","language","country","favoriteCategory"},
     *              @OA\Property(property="email", type="string", format="email", example="user@example.com"),
     *              @OA\Property(property="language", type="string", example="en"),
     *              @OA\Property(property="country", type="string", example="US"),
     *              @OA\Property(property="favoriteCategory", type="string", example="sports")
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",
     *          @OA\Header(
     *              header="Location",
     *              description="The URL to redirect back",
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *          @OA\MediaType(
     *              mediaType="text/html",
     *              @OA\Schema(
     *                  type="string"
     *
     *              )
     *          )
     *      )
     *     )
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse{

        $user=$request->user();
        // Validate the request data
        $request->validate([
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
            'favoriteCategory' => ['required', 'string'],
            'language' => ['required', 'string'],
            'country' => ['required', 'string']
        ]);

        $user->fill($request->validated());
        if ($user->isDirty('email')) {
            $user->email_verified_at = null;
        }

        $user->language=$request->input('language');
        $user->country=$request->input('country');
        $user->favoriteCategory=$request->input('favoriteCategory');
        $user->email=$request->input('email');

        $user->save();

        return Redirect::back()->with('status', 'profile-updated');


    }

    /**
     * Delete the user's account.
     */
    public function delete(Request $request): RedirectResponse{
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
        }

}
