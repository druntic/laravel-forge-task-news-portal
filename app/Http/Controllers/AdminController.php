<?php

namespace App\Http\Controllers;

use App\Models\Logs;
use App\Models\News;
use App\Models\User;
use App\Models\Comment;
use App\Models\UserLog;
use App\Models\UserComment;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ProfileUpdateRequest;
use Exception;

class AdminController extends Controller
{
    /**
     * @OA\Get(
     *      path="/profile/admin",
     *      operationId="getAdminPanelView",
     *      tags={"Admin"},
     *      summary="Get the admin panel view",
     *      description="Returns a view with user names and ids",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  properties={
     *                      "userNames"=@OA\Property(property="userNames", type="array", @OA\Items(type="string"), example={"dadp", "pero"}),
     *                      "userIds"=@OA\Property(property="userIds", type="array", @OA\Items(type="string"), example={"123e4567-e89b-12d3-a456-426614174000","123e4567-e89b-12d3-a456-426614174001"}),
     *
     *                  },
     *                  required={"userNames", "userIds", }
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal server error",
     *      )
     * )
     */

    public function panel(Request $request)
    {

        return view('profile.admin.admin', [
            'userNames' => User::pluck('name'),
            'userIds' => User::pluck('id')
        ]);
    }

    /**
     * @OA\Get(
     * path="/admin/users/{id}/comments",
     * operationId="getUserComments",
     * tags={"Admin"},
     * summary="Get all comments of a user",
     * description="Returns a view with user name, id and comments",
     * @OA\Response(
     * response=200,
     * description="Successful operation",
     * @OA\MediaType(
     * mediaType="application/json",
     * @OA\Schema(
     * type="object",
     * properties={
     * "name"=@OA\Property(property="name", type="string", example="dado"),
     * "id"=@OA\Property(property="id", type="string", example="123e4567-e89b-12d3-a456-426614174000"),
     * "comments"=@OA\Property(property="comments", type="integer", example="1"),
     * "dates"=@OA\Property(property="dates", type="integer", example="2023-05-08 11:00:44"),
     * "comments"=@OA\Property(property="comments", type="array",
     * @OA\Items(ref="#/components/schemas/UserComment"),
     * ),
     *
     * },
     * required={"name", "id", "comments", "dates", "titles", "comment_ids" }
     * )
     * )
     * ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Internal server error",
     *      )
     * )
     */

    public function comments(Request $request)
    {
        $id = $request->id;

        $name = User::where('id', $id)->pluck('name')[0];

        // Get all comments where user_id is equal to $id

        $comments = UserComment::where('user_id', $id)->get();

        return view('profile.admin.userComments', [
            'name' => $name,
            'id' => $id,
            'comments' => $comments->pluck('text')->reverse(),
            'dates' => $comments->pluck('created_at')->reverse(),
            'titles' => $comments->pluck('title')->reverse(),
            'comment_ids' => $comments->pluck('id')->reverse()
        ]);
    }

    /**
     * @OA\Put(
     *      path="/admin/users/{id}/comments",
     *      operationId="adminUpdateComment",
     *      tags={"Admin"},
     *      summary="Update a comment by id",
     *      description="Updates a comment of another user as an admin in the database and creates a log entry",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="The id of the comment to update",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="The new content of the comment",
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                      property="content",
     *                      type="string",
     *                      maxLength=255
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",

     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized user",

     *      ),
     *       @OA\Response(
     *           response=404,
     *           description="Comment not found",

     *       )
     *     )
     */
    public function updateComment(Request $request, $id)
    {

        // Validate the request data
        $request->validate([
            'id' => 'required|exists:user_comments_table,id',
            'content' => 'required|string|max:255',
        ]);
        // Find the comment by id
        $comment = UserComment::findOrFail($id);

        // Check if the comment belongs to the authenticated user


        if ($comment->user_id == auth()->id() or $request->user()->is_admin == true) {
            // Update the comment text
            $comment->text = $request->input('content');
            $comment->save();

            //create log
            UserLog::create([
                'user_id' => auth()->id(),
                'action' => 'update',
                'description' => "Admin modified comment on $comment->title"
            ]);

            // Redirect back with a success message
            return redirect()->back()->with('success', 'Comment updated successfully.');
        } else {
            // Return an error message
            return redirect()->back()->with('error', 'You are not authorized to update this comment.');
        }
    }
    /**
     * @OA\Delete(
     *      path="/admin/users/{id}/comments",
     *      operationId="adminDeleteComment",
     *      tags={"Admin"},
     *      summary="Delete a comment by id",
     *      description="Deletes a comment of a user as an admin from the database and creates a log entry",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="The id of the comment to delete",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Comment not found",
     *      )
     *     )
     */
    public function deleteComment($id)
    {
        // Find the comment by id
        $comment = UserComment::findOrFail($id);

        // Delete the comment from the database
        $comment->delete();

        //create log
        UserLog::create([
            'user_id' => auth()->id(),
            'action' => 'delete_comment',
            'description' => "Admin removed comment on $comment->title"
        ]);


        // Redirect back to the previous page with a success message
        return redirect()->back()->with('success', 'Comment deleted');
    }

    /**
     * @OA\Get(
     * path="/admin/users/{id}/favorites",
     * operationId="getFavoritesView",
     * tags={"Admin"},
     * summary="Get the favorites view",
     * description="Returns a view with news titles, ids, dates, images and urls",
     * @OA\Parameter(
     * name="id",
     * in="query",
     * description="The user id to filter the news by",
     * required=true,
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Successful operation",
     * @OA\MediaType(
     * mediaType="application/json",
     * @OA\Schema(
     * type="object",
     * properties={
     * "titles"=@OA\Property(property="titles", type="array", @OA\Items(type="string"), example={"News 1","News 2"}),
     * "newsIds"=@OA\Property(property="newsIds", type="array", @OA\Items(type="string"), example={"123e4567-e89b-12d3-a456-426614174123", "123e4567-e89b-12d3-a456-426614174567"}),
     * "dates"=@OA\Property(property="dates", type="array", @OA\Items(type="string"), example={"2021-11-19 02:39:44", "2021-11-19 02:39:01"}),
     * "images"=@OA\Property(property="images", type="array", @OA\Items(type="string"), example={"image1.jpg", "image2.jpg"}),
     * "urls"=@OA\Property(property="urls", type="array", @OA\Items(type="string"), example={"https://example.com/news1", "https://example.com/news2"}),
     * "id"=@OA\Property(property="id", type="string", format="uuid", example="123e4567-e89b-12d3-a456-426614174000"),
     *
     * },
     * required={"titles", "newsIds", "dates", "images", "urls", "id"}
     * )
     * )
     * ),
     * @OA\Response(
     * response=404,
     * description="Not found",
     * ),
     * @OA\Response(
     * response=500,
     * description="Internal server error",
     * )
     * )
     */
    public function favorites(Request $request)
    {

        $id = $request->id;
        $name = User::where('id', $id)->pluck('name')[0];

        // Get all news where user_id is equal to $id

        $news = News::where('user_id', $id)->get();


        return view('profile.admin.userFavorites', [
            'titles' => $news->pluck('title'),
            'newsIds' => $news->pluck('id'),
            'dates' => $news->pluck('created_at'),
            'images' => $news->pluck('image'),
            'urls' => $news->pluck('url'),
            'id' => $id
        ]);
    }

    /**
     * @OA\Delete(
     *      path="/admin/users/{id}/favorites",
     *      operationId="adminDeleteFavorite",
     *      tags={"Admin"},
     *      summary="Delete a news from users favorites as an admin",
     *      description="Deletes a news from the favorites as an admin and creates a log entry ",
     *      @OA\RequestBody(
     *          required=true,
     *          description="The id of the news to delete",
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                      property="id",
     *                      type="integer"
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized user",
     *      ),
     *       @OA\Response(
     *           response=404,
     *           description="News not found",
     *       )
     *     )
     */
    public function deleteFavorite(Request $request)
    {

        // Validate the request
        $request->validate([
            'id' => 'required|exists:user_favorites_table,id',
        ]);

        // Find the news by id
        $news = News::find($request->id);

        // Delete the news
        $news->delete();

        //create log
        UserLog::create([
            'user_id' => auth()->id(),
            'action' => 'delete_favorite',
            'description' => "Admin removed $news->title from favorites"
        ]);

        // Redirect back with a success message
        return redirect()->back()->with('success', 'News deleted successfully.');
    }

    /**
     * @OA\Get(
     *      path="admin/users/{id}/logs",
     *      operationId="getLogsView",
     *      tags={"Admin"},
     *      summary="Get the logs view",
     *      description="Returns a view with user logs details",
     * @OA\Parameter(
     * name="id",
     * in="query",
     * description="The user id",
     * required=true,
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Successful operation",
     * @OA\MediaType(
     * mediaType="application/json",
     * @OA\Schema(
     * type="object",
     *                  properties={
     *                      "description"=@OA\Property(property="description", type="array", @OA\Items(type="string"), example={"Created a news article", "Updated a news article"}),
     *                      "action"=@OA\Property(property="action", type="array", @OA\Items(type="string"), example={"create", "update"}),
     *                      "date"=@OA\Property(property="date", type="array", @OA\Items(type="string"), example={"2021-11-19 02:39:44", "2021-11-19 02:39:01"}),
     *                      "id"=@OA\Property(property="id", type="string", example="123e4567-e89b-12d3-a456-426614174000"),
     *
     *                  },
     *                  required={"description", "action", "date", "id"}
     * )
     * )
     * ),
     * @OA\Response(
     * response=404,
     * description="Not found",
     * ),
     * @OA\Response(
     * response=500,
     * description="Internal server error",
     * )
     * )
     */
    public function logs(Request $request)
    {
        $id = $request->id;
        $logs = UserLog::where('user_id', $id)->get();

        return view('profile.admin.userLogs', [
            'description' => $logs->pluck('description'),
            'action' => $logs->pluck('action'),
            'date' => $logs->pluck('created_at'),
            'id' => $id
        ]);
    }

    /**
     * @OA\Get(
     * path="admin/users/{id}",
     * operationId="getInformationView",
     * tags={"Admin"},
     * summary="Get the information view of a specific user",
     * description="Returns a view with news titles, ids, dates, images and urls",
     * @OA\Parameter(
     * name="id",
     * in="query",
     * description="The user id ",
     * required=true,
     * @OA\Schema(
     * type="string"
     * )
     * ),
     * @OA\Response(
     * response=200,
     * description="Successful operation",
     * @OA\MediaType(
     * mediaType="application/json",
     * @OA\Schema(
     * type="object",
     * properties={
     * "titles"=@OA\Property(property="titles", type="array", @OA\Items(type="string"), example={"News 1","News 2"}),
     * "newsIds"=@OA\Property(property="newsIds", type="array", @OA\Items(type="string"), example={"123e4567-e89b-12d3-a456-426614174123","123e4567-e89b-12d3-a456-426614174567"}),
     * "dates"=@OA\Property(property="dates", type="array", @OA\Items(type="string"), example={"2021-11-19 02:39:44", "2021-11-19 02:39:01"}),
     * "images"=@OA\Property(property="images", type="array", @OA\Items(type="string"), example={"image1.jpg", "image2.jpg"}),
     * "urls"=@OA\Property(property="urls", type="array", @OA\Items(type="string"), example={"https://example.com/news1", "https://example.com/news2"}),
     * "id"=@OA\Property(property="id", type="string", example="123e4567-e89b-12d3-a456-426614174000"),
     *
     * },
     * required={"titles", "newsIds", "dates", "images", "urls", "id"}
     * )
     * )
     * ),
     * @OA\Response(
     * response=404,
     * description="Not found",
     * ),
     * @OA\Response(
     * response=500,
     * description="Internal server error",
     * )
     * )
     */
    public function information(Request $request)
    {
        $id = $request->id;
        $user = User::where('id', $id)->first();
        // Create the languages array
        $languages = config('constants.languages');
        $countries = config('constants.countries');
        $categories = config('constants.categories');


        // Temporarily log in as the $user
        //Auth::login($user);
        return view('profile.admin.edit', [
            'user' => $user,
            'languages' => $languages,
            'countries' => $countries,
            'categories' => $categories
        ]);
    }
    /**
     * @OA\Patch(
     *      path="/admin/users/{id}",
     *      operationId="adminUpdateUserProfile",
     *      tags={"Admin"},
     *      summary="Update the user's profile information",
     *      description="Returns a redirect response with status message",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="The id of the user",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"email","language","country","favoriteCategory"},
     *              @OA\Property(property="email", type="string", format="email", example="user@example.com"),
     *              @OA\Property(property="language", type="string", example="en"),
     *              @OA\Property(property="country", type="string", example="us"),
     *              @OA\Property(property="favoriteCategory", type="string", example="sports")
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",
     *          @OA\Header(
     *              header="Location",
     *              description="The URL to redirect back",
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      )
     *     )
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $user = User::where('id', $request->route('id'))->first();

        // Validate the request data

        $request->validate([
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);

        $user->fill($request->validated());
        if ($user->isDirty('email')) {
            $user->email_verified_at = null;
        }

        $user->language = $request->input('language');
        $user->country = $request->input('country');
        $user->favoriteCategory = $request->input('favoriteCategory');
        $user->email = $request->input('email');
        $user->save();

        return Redirect::back()->with('status', 'profile-updated');
    }

    /**
     * @OA\Delete(
     *      path="/admin/users/{id}",
     *      operationId="adminDeleteUserAccount",
     *      tags={"Admin"},
     *      summary="Delete the user's account",
     *      description="Returns a redirect response to admin profile",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="The id of the user",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",
     *          @OA\Header(
     *              header="Location",
     *              description="The URL to redirect to admin profile",
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Not found",
     *      )
     *     )
     */
    public function delete(Request $request): RedirectResponse
    {
        $user = User::where('id', $request->route('id'))->first();
        $user->delete();
        return Redirect::to('/profile/admin');
    }
}
