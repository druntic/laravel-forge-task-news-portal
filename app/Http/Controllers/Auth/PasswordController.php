<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\Rules\Password;

class PasswordController extends Controller
{
    /**
     * Update the user's password.
     */
    public function update(Request $request): RedirectResponse
    {
        $user=$request->user();
        if($request->user()->is_admin){
            $user=User::where('id', $request->input('id'))->first();
            $password=$request->input('password');
            $user->update([
                'password' => Hash::make($password),
            ]);
        }
        else{
            $user=$request->user();
            $validated = $request->validateWithBag('updatePassword', [
                'current_password' => ['required', 'current_password'],
                'password' => ['required', Password::defaults()->symbols(), 'confirmed'],
            ]);

        $user->update([
            'password' => Hash::make($validated['password']),
        ]);

        }



        return back()->with('status', 'password-updated');
    }
}

