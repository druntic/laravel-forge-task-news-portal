<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\RedirectResponse;

class VerifyEmailController extends Controller
{
/* *

@OA\Post(
path=“/email/verification-notification”,
operationId=“markEmailAsVerified”,
tags={“Auth”},
summary=“Mark the authenticated user’s email address as verified”,
description=“Sends a verification notification to the user’s email and redirects to home”,
@OA\Response(
response=302,
description=“Successful operation”
),
@OA\Response(
response=401,
description=“Unauthenticated”
),
@OA\Response(
response=403,
description=“Forbidden”
),
@OA\Response(
response=422,
description=“Validation error”
)
) */
    public function __invoke(EmailVerificationRequest $request): RedirectResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return redirect()->intended(RouteServiceProvider::HOME.'?verified=1');
    }
}
