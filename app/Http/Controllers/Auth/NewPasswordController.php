<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\View\View;
use App\Rules\PasswordRule;

class NewPasswordController extends Controller
{
    /**
     * @OA\Get(
     *      path="/reset-password/{token}",
     *      operationId="createNewPassword",
     *      tags={"Auth"},
     *      summary="Display the password reset view",
     *      description="Returns a view with a form to reset the password",
     *      @OA\Parameter(
     *          name="token",
     *          in="path",
     *          required=true,
     *          description="The password reset token",
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=302,
     *          description="Redirect to home if already logged in"
     *      )
     * )
     */
    public function create(Request $request): View
    {
        return view('auth.reset-password', ['request' => $request]);
    }

    /**
     * @OA\Post(
     *      path="/reset-password",
     *      operationId="storeNewPassword",
     *      tags={"Auth"},
     *      summary="Handle an incoming new password request",
     *      description="Resets the user's password and returns a redirect response",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"token", "email", "password", "password_confirmation"},
     *              @OA\Property(property="token", type="string", description="The password reset token", example="a1b2c3d4e5f6g7h8i9j0"),
     *              @OA\Property(property="email", type="string", format="email", example="user@example.com"),
     *              @OA\Property(property="password", type="string", format="password", example="P@ssw0rd"),
     *              @OA\Property(property="password_confirmation", type="string", format="password", example="P@ssw0rd")
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation or validation error"
     *       )
     * )
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'token' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()->symbols(), ],
        ]);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($request->password),
                    'remember_token' => Str::random(60),
                ])->save();

                event(new PasswordReset($user));
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $status == Password::PASSWORD_RESET
                    ? redirect()->route('login')->with('status', __($status))
                    : back()->withInput($request->only('email'))
                            ->withErrors(['email' => __($status)]);
    }
}
