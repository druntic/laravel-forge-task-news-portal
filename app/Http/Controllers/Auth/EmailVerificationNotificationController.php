<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EmailVerificationNotificationController extends Controller
{
    /**
     * Send a new email verification notification to the user.
     *
     * @OA\Post(
     *     path="/email/verification-notification",
     *     operationId="sendVerificationNotification",
     *     tags={"Auth"},
     *     summary="Send a new email verification notification to the user",
     *     description="Sends a new email verification notification to the user's email address if it has not yet been verified.",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response="302",
     *         description="Successfully sent email verification notification and redirected back"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized: user is not authenticated"
     *     ),
     * )
     */
    public function store(Request $request): RedirectResponse
    {
        if ($request->user()->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::HOME);
        }

        $request->user()->sendEmailVerificationNotification();

        return back()->with('status', 'verification-link-sent');
    }
}
