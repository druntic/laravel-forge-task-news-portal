<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * @OA\Get(
     *      path="/register",
     *      operationId="showRegistrationForm",
     *      tags={"Auth"},
     *      summary="Display the registration view",
     *      description="Returns a view with a registration form",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=302,
     *          description="Redirect to home if already logged in"
     *      )
     * )
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * @OA\Post(
     *      path="/register",
     *      operationId="registerUser",
     *      tags={"Auth"},
     *      summary="Handle an incoming registration request",
     *      description="Creates a new user and redirects to the home page",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string", example="John Doe"),
     *              @OA\Property(property="email", type="string", format="email", example="johndoe@example.com"),
     *              @OA\Property(property="password", type="string", format="password", example="secret"),
     *              @OA\Property(property="password_confirmation", type="string", format="password", example="secret")
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",
     *          @OA\Header(
     *              header="Location",
     *              description="The URL of the home page",
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Validation error",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The given data was invalid."),
     *              @OA\Property(property="errors", type="object",
     *                  @OA\Property(property="name", type="array",
     *                      @OA\Items(type="string")
     *                  ),
     *                  @OA\Property(property="email", type="array",
     *                      @OA\Items(type="string")
     *                  ),
     *                  @OA\Property(property="password", type="array",
     *                      @OA\Items(type="string")
     *                  )
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()->symbols()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'language' => 'en',
            'country' => 'us',
            'favoriteCategory' => 'general'

        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect('/');
    }
}
