<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\View\View;

class PasswordResetLinkController extends Controller
{
    /**
     * @OA\Get(
     *      path="/forgot-password",
     *      operationId="create",
     *      tags={"Auth"},
     *      summary="Display the password reset link request view",
     *      description="Returns a view with a form to request a password reset link",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @OA\Response(
     *          response=302,
     *          description="Redirect to home if already logged in"
     *      )
     * )
     */
    public function create(): View
    {
        return view('auth.forgot-password');
    }

    /**
     * @OA\Post(
     *      path="/forgot-password",
     *      operationId="store",
     *      tags={"Auth"},
     *      summary="Handle an incoming password reset link request",
     *      description="Sends a password reset link to the user's email address and returns a redirect response",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"email"},
     *              @OA\Property(property="email", type="string", format="email", example="user@example.com")
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation or validation error"
     *       )
     * )
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status == Password::RESET_LINK_SENT
                    ? back()->with('status', __($status))
                    : back()->withInput($request->only('email'))
                            ->withErrors(['email' => __($status)]);
    }
}
