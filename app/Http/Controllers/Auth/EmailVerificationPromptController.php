<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification page or redirect to home if email is already verified.
     *
     * @OA\Get(
     *     path="/email/verify",
     *     operationId="verifyEmailPage",
     *     tags={"Auth"},
     *     summary="Display the email verification page or redirect to home if email is already verified",
     *     description="Displays the email verification page if the user's email address is not yet verified; otherwise, redirects the user to the home page.",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Successfully displayed the email verification page"
     *     ),
     *     @OA\Response(
     *         response="302",
     *         description="Redirected to home page because email address has already been verified"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized: user is not authenticated"
     *     ),
     * )
     */
    public function __invoke(Request $request): RedirectResponse|View
    {
        return $request->user()->hasVerifiedEmail()
                    ? redirect()->intended(RouteServiceProvider::HOME)
                    : view('auth.verify-email');
    }
}
