<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * @OA\Get(
     *      path="/login",
     *      operationId="showLoginForm",
     *      tags={"Auth"},
     *      summary="Display the login view",
     *      description="Returns a view with a login form",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=302,
     *          description="Redirect to home if already logged in",
     *      )
     * )
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * @OA\Post(
     *      path="/login",
     *      operationId="loginUser",
     *      tags={"Auth"},
     *      summary="Handle an incoming authentication request",
     *      description="Authenticates the user and redirects to the intended page",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="email", type="string", format="email", example="johndoe@example.com"),
     *              @OA\Property(property="password", type="string", format="password", example="secret"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",

     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Validation error",
     *
     *      )
     * )
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->intended('/');
    }

    /**
     * Logout the authenticated user.
     *
     * @OA\Post(
     *     path="/logout",
     *     operationId="logoutUser",
     *     tags={"Auth"},
     *     summary="Logout the authenticated user",
     *     description="Destroys the user's session and logs them out",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response="302",
     *         description="Successfully logged out and redirected to home page"
     *     ),
     * )
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
