<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ConfirmablePasswordController extends Controller
{
    /**
     * Show the confirm password view.
     *
     * @OA\Get(
     *     path="/confirm-password",
     *     operationId="showConfirmPassword",
     *     tags={"Auth"},
     *     summary="Display the confirm password view",
     *     description="Displays the confirm password view to allow the user to re-authenticate before performing sensitive actions.",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response="200",
     *         description="Successfully displayed the confirm password view"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized: user is not authenticated"
     *     ),
     * )
     */
    public function show(): View
    {
        return view('auth.confirm-password');
    }

    /**
     * Confirm the user's password.
     *
     * @OA\Post(
     *     path="/confirm-password",
     *     operationId="confirmPassword",
     *     tags={"Auth"},
     *     summary="Confirm the user's password",
     *     description="Confirms the user's password to allow the user to perform sensitive actions.",
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         description="The user's password",
     *         @OA\JsonContent(
     *             required={"password"},
     *             @OA\Property(property="password", type="string")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Password confirmed successfully"
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized: user is not authenticated or password is incorrect"
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Unprocessable entity: validation error"
     *     ),
     * )
     */
    public function store(Request $request): RedirectResponse
    {
        if (! Auth::guard('web')->validate([
            'email' => $request->user()->email,
            'password' => $request->password,
        ])) {
            throw ValidationException::withMessages([
                'password' => __('auth.password'),
            ]);
        }

        $request->session()->put('auth.password_confirmed_at', time());

        return redirect()->intended('/');
    }
}
