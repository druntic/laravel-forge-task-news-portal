<?php

namespace App\Http\Controllers;

use App\Models\Logs;
use App\Models\News;
use App\Models\User;

use App\Models\UserComment;
use Exception;
use Illuminate\Http\Request;
use jcobhams\NewsApi\NewsApi;
use Illuminate\Support\Facades\Log;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\DocBlock\Tag;

class NewsController extends Controller
{
    /**
        * @OA\Get(
        *      path="/",
        *      operationId="indexNews",
        *      tags={"News"},
        *      summary="Display the top headlines on the main page",
        *      description="Returns a view with the top headlines focused around the favorite category of the user",
        *      @OA\Parameter(
        *          name="category",
        *          in="query",
        *          required=false,
        *          description="The category of the news to display",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="pageSize",
        *          in="query",
        *          required=false,
        *          description="The number of news to display per page",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="sort",
        *          in="query",
        *          required=false,
        *          description="The sort order of the news",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="search",
        *          in="query",
        *          required=false,
        *          description="The search query for the news",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="page",
        *          in="query",
        *          required=false,
        *          description="The page number of the news to display",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="source[]",
        *          in="query",
        *          required=false,
        *          description="The sources of the news to display",
        *          @OA\Schema(
        *              type="array",
        *              items={
        *                  @OA\Property(
        *                      type="string"
        *                  )
        *              }
        *
        *          )
        *      ),
        *      @OA\Response(
        *          response=200,
        *          description="Successful operation",
        *          @OA\JsonContent(ref="#/components/schemas/News")
        *       ),
        *      @OA\Response(
        *          response=401,
        *          description="Unauthorized user",
        *      ),
        *       @OA\Response(
        *           response=404,
        *           description="Not found",
        *       )
        *     )
        */
    public function index(Request $request){

        $newsapi = new NewsApi(config('services.newsapi.key'));
        $country=Auth::user()->country;
        $category=$request->collect()->get('category');
        if($category == null){
            $category = Auth::user()->favoriteCategory;
        }
        $page_size=$request->collect()->get('pageSize') ?? 5;
        $sources=null;
        $sort_by=$request->collect()->get('sort');

        if($sort_by=="none"){
            $sort_by=null;
        }

        //current uri
        $uri=$request->getRequestUri();
        //remove tags
        $uri=explode('?', $uri)[0];
        $q=$request->input('search');

        //get the input value from the hidden field
        $page=(int)$request->collect()->get('page');

        //the api wont work if the search word is empty
        if ($q==null){
            $q='news';
        }
        if($page==0){
            $page=1;
        }
        $sourcesArray=$request->input('source');

        //turn the array of sources into one string
        if(is_string($sourcesArray) or is_null($sourcesArray)){
            $sources=$sourcesArray;
        }


        else{
            $sources = "";
            for ($i = 0; $i < count($sourcesArray); $i++) {
                if ($i == count($sourcesArray) - 1) {
                    $sources .= $sourcesArray[$i];
                } else {
                    $sources .= $sourcesArray[$i] . ",";
                }
            }
        }

        //$sourcesArray must be an array of strings for checkboxes
        if (is_string($sourcesArray)){
            $sourcesArray=explode(',', $sourcesArray);
        }
        else if (is_null($sourcesArray)){
            $sourcesArray=[];
        }

        // Get the sources from the API
        $displaySources = $newsapi->getSources($category, Auth::user()->language, $country);
        //you cant use sources and category/country at the same time
        if ($sources != null){
            //comment this and replace with 100 if you are hitting request limit
            $all_news = $newsapi->getTopHeadlines($q, $sources, null, null, $page_size, $page);

            //call the api again in order to get the total number of news(missing on 3rd party api)
            $number_of_news=count($newsapi->getTopHeadlines($q, $sources, null, null, 100, 1)
            ->articles);
        }

        else{
            //comment this and replace with 100 if you are hitting request limit
            $all_news = $newsapi->getTopHeadlines($q, $sources, $country, $category, $page_size, $page);

            //call the api again in order to get the total number of news(missing on 3rd party api)
            $number_of_news=count($newsapi->getTopHeadlines($q, $sources, $country, $category, 100, 1)
            ->articles);
        }


        // Create a collection from the sources array
        $collection = collect($displaySources->sources);

        // Pluck the name attribute from each source
        $names = $collection->pluck('name');
        $ids = $collection ->pluck('id');

        $number_of_pages=ceil($number_of_news/$page_size);

        //find the number of comments for every news
        $all_urls = array_column($all_news->articles, 'url');
        // Assuming $all_urls is an array of urls
        $comments = [];
        foreach ($all_urls as $i => $url) {
            $comments[$i] = count(UserComment::where('url', $url)->get());
        }

        return view('news.index',[
            'news'=>$all_news->articles,
            'pageCount'=>$number_of_pages,
            'search'=>$q,
            'sort'=>$sort_by,
            'categories' => $newsapi->getCategories(),
            'category' => $category,
            'page' => $page,
            'uri' => $uri,
            'comments' => $comments,
            'country' => $country,
            'category' => $category,
            'name' => $names,
            'sourceId' => $ids,
            'source' => $sources,
            'from' => null,
            'to' => null,
            'sourceArray' => $sourcesArray,
            'pageSize' => $page_size

        ]);



    }

    /**
        * @OA\Get(
        *      path="/all-news",
        *      operationId="allNews",
        *      tags={"News"},
        *      summary="Display all the news on the page",
        *      description="Returns a view with all the news filtered by the user's preferences",
        *      @OA\Parameter(
        *          name="category",
        *          in="query",
        *          required=false,
        *          description="The category of the news to display",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="pageSize",
        *          in="query",
        *          required=false,
        *          description="The number of news to display per page",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="sort",
        *          in="query",
        *          required=false,
        *          description="The sort order of the news",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="search",
        *          in="query",
        *          required=false,
        *          description="The search query for the news",
        *          @OA\Schema(
        *              type="string"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="page",
        *          in="query",
        *          required=false,
        *          description="The page number of the news to display",
        *          @OA\Schema(
        *              type="integer"
        *          )
        *      ),
        *      @OA\Parameter(
        *          name="source[]",
        *          in="query",
        *          required=false,
        *          description="The sources of the news to display",
        *          @OA\Schema(
        *              type="array",
        *              items={
        *                  @OA\Property(
        *                      type="string"
        *                  )
        *              }
        *
        *          )
        *      ),
        *      @OA\Response(
        *          response=200,
        *          description="Successful operation",
        *          @OA\JsonContent(ref="#/components/schemas/News")
        *       ),
        *      @OA\Response(
        *          response=401,
        *          description="Unauthorized user",
        *      ),
        *       @OA\Response(
        *           response=404,
        *           description="Not found",
        *       )
        *     )
        */
    public function allNews(Request $request){

        $newsapi = new NewsApi(config('services.newsapi.key'));
        $category=Auth::user()->favoriteCategory;
        $page_size=$request->collect()->get('pageSize') ?? 5;
        $language=Auth::user()->language;
        $sort_by=$request->collect()->get('sort');
        $domains=null;
        $exclude_domains=null;
        $from=$request->input('from');
        $to=$request->input('to');
        if($sort_by=="none"){
            $sort_by=null;
        }
        $q=$request->input('search');
        //current uri
        $uri=$request->getRequestUri();
        //remove tags
        $uri=explode('?', $uri)[0];


        $page=(int)$request->collect()->get('page');

        //the api wont work if the search word is empty
        if ($q==null){
            $q='news';
        }
        if($page==0){
            $page=1;
        }

        $sourcesArray=$request->input('source');

        //turn the array of sources into one string
        if(is_string($sourcesArray) or is_null($sourcesArray)){
            $sources=$sourcesArray;
        }


        else{
            $sources = "";
            for ($i = 0; $i < count($sourcesArray); $i++) {
                if ($i == count($sourcesArray) - 1) {
                    $sources .= $sourcesArray[$i];
                } else {
                    $sources .= $sourcesArray[$i] . ",";
                }
            }
        }

        //$sourcesArray must be an array of strings for checkboxes
        if (is_string($sourcesArray)){
            $sourcesArray=explode(',', $sourcesArray);
        }
        else if (is_null($sourcesArray)){
            $sourcesArray=[];
        }


        // Get the sources from the API
        $displaySources = $newsapi->getSources($category, $language, Auth::user()->country);

        // Create a collection from the sources array
        $collection = collect($displaySources->sources);

        // Pluck the name attribute from each source
        $names = $collection->pluck('name');
        $ids = $collection ->pluck('id');


        $all_news = $newsapi->getEverything($q, $sources,$domains, $exclude_domains,
        $from, $to, $language, $sort_by, $page_size, $page);
        //api cant return more than 100 articles per page
        $number_of_news=count($newsapi->getEverything($q, $sources,$domains, $exclude_domains,
        $from, $to, $language, $sort_by, 100, 1)->articles);

        $number_of_pages=ceil($number_of_news/$page_size);


        //find the number of comments for every news
        $all_urls = array_column($all_news->articles, 'url');
        // Assuming $all_urls is an array of urls
        $comments = [];
        foreach ($all_urls as $i => $url) {
            $comments[$i] = count(UserComment::where('url', $url)->get());
        }


        return view('news.allNews',[
            'news'=>$all_news->articles,
            'pageCount'=>$number_of_pages,
            'search'=>$q,
            'sort'=>$sort_by,
            'categories' => $newsapi->getCategories(),
            'category' => $category,
            'page' => $page,
            'uri' => $uri,
            'comments' => $comments,
            'name' => $names,
            'sourceId' => $ids,
            'source' => $sources,
            'sourceArray' => $sourcesArray,
            'from' => $from,
            'to' => $to,
            'pageSize' => $page_size

        ]);

    }

    public function get(Request $request){

        $newsapi = new NewsApi(config('services.newsapi.key'));
        $page_size=5;
        $page=1;
        $language=null;
        $sources=null;
        $sort_by=null;
        $from=null;
        $to=null;
        $domains=null;
        $exclude_domains=null;
        $q=$request->title;
        $category=$request->category;
        $country=$request->country;

        //its possible that one search wont give a unique news
        //we will get the unique news using the url
        $trueUrl=hex2bin($request->url);

        //getEverything and getTopHeadLines do not share news!

        //if we are getting news from top headlines use top headlines api call
        //else use get everything
        if ($request->isTop){
            $all_news = $newsapi->getTopHeadlines($q, $sources, $country, $category, $page_size, $page);
        }
        else{
            $all_news = $newsapi->getEverything($q, $sources,$domains, $exclude_domains,
            $from, $to, $language, $sort_by, $page_size, $page);
        }


        // Get all the comments which have the same url as our news
        //it can happen that we get more than one news with the identical title
        //we will loop over all the news with identical titles and get
        //the one which has the true url

        // Initialize $news as null
        $news = null;


        // Loop over the articles array
        foreach ($all_news->articles as $article) {
            // Check if the url property matches the $trueUrl variable
            if ($article->url == $trueUrl) {
                // Assign the matching article to $news
                $news = $article;
                // Break the loop
                break;
            }
        }


        $url = $news->url;
        $comments = UserComment::where('url', $url)->get();
        $users = $comments->map(function ($comment) {
            return $comment->user; // get the user who wrote each comment
        });


        //pass the reversed arrays so the first comment is always on top
        return view('news.oneNews',[
            'news'=>$all_news->articles,
            'text' => $comments->pluck('text')->reverse(),
            'title' => $comments->pluck('title')->reverse(),
            'users' => $users->pluck('name')->reverse(),
            'dates' => $comments->pluck('created_at')->reverse(),
            'user_ids'=> $users->pluck('id')->reverse(),
            'comment_ids' => $comments->pluck('id')->reverse(),
            'comment_times' => $comments->pluck('updated_at')->reverse()

        ]);

    }
}
