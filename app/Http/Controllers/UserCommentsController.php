<?php

namespace App\Http\Controllers;
use App\Models\UserLog;
use App\Models\UserComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserCommentsController extends Controller
{
    /**
     * @OA\Post(
     *      path="/comments",
     *      operationId="storeComment",
     *      tags={"Comments"},
     *      summary="Store a new comment",
     *      description="Returns a redirect response with success message",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"url","title","text"},
     *              @OA\Property(property="url", type="string", format="url", example="https://example.com/news/1"),
     *              @OA\Property(property="title", type="string", example="Breaking news"),
     *              @OA\Property(property="text", type="string", example="This is my comment")
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",
     *          @OA\Header(
     *              header="Location",
     *              description="The URL to redirect back",
     *              @OA\Schema(
     *                  type="string"
     *              )
     *          )
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Validation error",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The given data was invalid."),
     *              @OA\Property(property="errors", type="object", example={
     *                  "url": {"The url field is required."},
     *                  "title": {"The title field is required."},
     *                  "text": {"The text field is required."}
     *              })
     *          )
     *      )
     *     )
     */
    public function store(Request $request){
        // Validate the request data
        $request->validate([
            'url' => 'required|url',
            'title' => 'required|string',
            'text' => 'required|string',
        ]);

        // Create a new comment instance and fill it with data

        UserComment::create([
            'url' => $request->url,
            'title' => $request->title,
            'text' => $request->text,
            'user_id' => auth()->id(),
        ]);


        //create log
        $user_id=auth()->id();
        $news_title=$request->title;
        UserLog::create([
            'user_id' => $user_id,
            'action' => 'commented',
            'description' => "User commented on $news_title"
        ]);

        // Redirect back with a success message
        return redirect()->back()->with('success', 'Comment submitted successfully.');
    }
    /**
     * @OA\Delete(
     *      path="/comments/{id}",
     *      operationId="deleteComment",
     *      tags={"Comments"},
     *      summary="Delete a comment by id",
     *      description="Deletes a comment from the database and creates a log entry",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="The id of the comment to delete",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",

     *       ),
     *      @OA\Response(
     *          response=404,
     *          description="Comment not found",

     *      )
     *     )
     */
    public function delete($id){

        // Find the comment by id
        $comment = UserComment::findOrFail($id);
        //Check if the time difference is less than 300 seconds
        if($comment->updated_at->diffInSeconds(now())<3){
            // Delete the comment from the database
            $comment->delete();
            //create log
            $user_id=auth()->id();
            $news_title=$comment->title;
            UserLog::create([
                'user_id' => $user_id,
                'action' => 'delete_comment',
                'description' => "User removed comment on $news_title"
            ]);


            // Redirect back to the previous page with a success message
            return redirect()->back()->with('success', 'Comment deleted');
        }
        else{
            return redirect()->back()->with(session()->flash('error', 'Comment cannot be deleted after 300 seconds'));
        }
    }

    /**
     * @OA\Put(
     *      path="/comments/{id}",
     *      operationId="updateComment",
     *      tags={"Comments"},
     *      summary="Update a comment by id",
     *      description="Updates a comment in the database and creates a log entry if the user is authorized",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="The id of the comment to update",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          description="The new content of the comment",
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                      property="content",
     *                      type="string",
     *                      maxLength=255
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",

     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized user",

     *      ),
     *       @OA\Response(
     *           response=404,
     *           description="Comment not found",

     *       )
     *     )
     */
    public function update(Request $request, $id){

        // Validate the request data
        $request->validate([
            'id' => 'required|exists:user_comments_table,id',
            'content' => 'required|string|max:255',
        ]);
        // Find the comment by id
        $comment = UserComment::findOrFail($id);
        //Check if the time difference is less than 300 seconds
        if($comment->updated_at->diffInSeconds(now())<300){
                    // Check if the comment belongs to the authenticated user
            if ($comment->user_id == auth()->id() or $request->user()->is_admin == true) {
                // Update the comment text
                $comment->text = $request->input('content');
                $comment->save();

                //create log
                $user_id=auth()->id();
                $news_title=$comment->title;
                UserLog::create([
                    'user_id' => $user_id,
                    'action' => 'update',
                    'description' => "User modified comment on $news_title"
                ]);

                // Redirect back with a success message
                return redirect()->back()->with('success', 'Comment updated successfully.');
            } else {
                // Return an error message
                return redirect()->back()->with('error', 'You are not authorized to update this comment.');
            }
        }
        else{
            return redirect()->back()->with(session()->flash('error', 'Comment cannot be updated after 300 seconds'));

        }
    }


}
