<?php

namespace App\Http\Controllers;

use App\Models\UserLog;

use App\Models\News;
use App\Models\User;
use Illuminate\Http\Request;

class UserFavoritesController extends Controller
{
    /**
* @OA\Get(
* path="user/favorites",
* operationId="getFavoriteNews",
* tags={"Favorites"},
* summary="Get the favorite news of the authenticated user",
* description="Returns a view with news titles, ids, dates, images, descriptions and urls",

    * @OA\Response(
    * response=200,
    * description="Successful operation",
    * @OA\MediaType(
    * mediaType="application/json",
    * @OA\Schema(
    * type="object",
    * properties={
    *"images"=@OA\Property(property="images", type="array", @OA\Items(type="string"), example={"image1.jpg", "image2.jpg"}),
    *"dates"=@OA\Property(property="dates", type="array", @OA\Items(type="string"), example={"2021-11-19 02:39:44", "2021-11-19 02:39:01"}),
    *"descriptions"=@OA\Property(property="descriptions", type="array", @OA\Items(type="string"), example={"This is a news article.", "This is another news article."}),
    *"urls"=@OA\Property(property="urls", type="array", @OA\Items(type="string"), example={"https://example.com/news1", "https://example.com/news2"}),
    *"ids"=@OA\Property(property="ids", type="array", @OA\Items(type="string",format="uuid"), example={"123e4567-e89b-12d3-a456-426614174001", "123e4567-e89b-12d3-a456-426614174000"}),
    *"newsIds"=@OA\Property(property="newsIds", type="array", @OA\Items(type="string"), example={"123e4567-e89b-12d3-a456-426614174123", "123e4567-e89b-12d3-a456-426614174567"}),
    *
    * },
    * required={"titles", "images", "dates", "descriptions", "urls", "ids", "newsIds"}
    * )
    * )
    * ),
    * @OA\Response(
    * response=401,
    * description="Unauthenticated",
    * ),
    * @OA\Response(
    * response=404,
    * description="User not found",
    * ),
    * @OA\Response(
    * response=500,
    * description="Internal server error",
    * )
    * )
    */
    public function get(Request $request){
        $user = User::findOrFail(auth()->id());
        //this will be used for deleting news from favorites

        return view('news.favoriteNews',[
            'titles' => $user->news_favorites()->pluck('title'),
            'images' => $user->news_favorites()->pluck('image'),
            'dates' =>$user->news_favorites()->pluck('created_at'),
            'descriptions' => $user->news_favorites()->pluck('description'),
            'urls' => $user->news_favorites()->pluck('url'),
            'ids' => range(1, count($user->news_favorites()->pluck('title'))),
            //we will use this to delete news
            'newsIds' => $user->news_favorites()->pluck('id'),

        ]);
    }
    /**
     * @OA\Post(
     *      path="/users/favorites",
     *      operationId="createFavorite",
     *      tags={"Favorites"},
     *      summary="Save a news to users favorites",
     *      description="Creates a new NewsFavorite instance and a log entry if the user is authorized and the news url is not already in the favorites",
     *      @OA\RequestBody(
     *          required=true,
     *          description="The data of the news to save",
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(
     *                      property="news_url",
     *                      type="string",
     *                      format="uri"
     *                  ),
     *                  @OA\Property(
     *                      property="news_title",
     *                      type="string"
     *                  ),
     *                  @OA\Property(
     *                      property="news_image",
     *                      type="string",
     *                      format="uri"
     *                  ),
     *                  @OA\Property(
     *                      property="news_description",
     *                      type="string"
     *                  )
     *               )
     *           )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",

     *       ),
     *       @OA\Response(
     *          response=401,
     *          description="Unauthorized user",

     *      ),
     *       @OA\Response(
     *            response=404,
     *           description="User not found",

     *        )
     *     )
     */
    public function create(Request $request){

        // Get the current user or fail if not found
        $user = User::findOrFail(auth()->id());

        //check if url is in table before adding it
        $Urls=$user->news_favorites()->pluck('url')->toArray();
        if(in_array($request->get('news_url'),$Urls)){
            return back();
        }

        // Create a new NewsFavorite instance and fill it with data

        $user_id=auth()->id();
        $news_title=$request->get('news_title');
        News::create([
            'url' => $request->get('news_url'),
            'title' => $news_title,
            'image' => $request->get('news_image'),
            'description' => $request->get('news_description'),
            'user_id' => $user_id

        ]);

        //create log
        UserLog::create([
            'user_id' => $user_id,
            'action' => 'favorited',
            'description' => "User favorited $news_title"
        ]);

        return back();
    }

    /**
     * @OA\Delete(
     *      path="/users/favorites",
     *      operationId="deleteFavorite",
     *      tags={"Favorites"},
     *      summary="Delete a news from users favorites",
     *      description="Deletes a news from the favorites and creates a log entry if the user is authorized",
     *      @OA\RequestBody(
     *          required=true,
     *          description="The id of the news to delete",
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",

     *          )
     *      ),
     *      @OA\Response(
     *          response=302,
     *          description="Successful operation",

     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthorized user",

     *      ),
     *       @OA\Response(
     *           response=404,
     *           description="News not found",

     *       )
     *     )
     */
    public function delete (Request $request) {

        // Validate the request
        $request->validate([
            'id' => 'required|exists:user_favorites_table,id',
        ]);

        // Find the news by id
        $news = News::findOrFail($request->id);

        // Delete the news
        $news->delete();


        //create log
        $user_id=auth()->id();
        $news_title=$news->title;
        UserLog::create([
            'user_id' => $user_id,
            'action' => 'delete_favorite',
            'description' => "User removed $news_title from favorites"
        ]);


        // Redirect back with a success message
        return redirect()->back()->with('success', 'News deleted successfully.');
    }

}
